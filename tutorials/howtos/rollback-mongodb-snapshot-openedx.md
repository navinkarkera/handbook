# Rollback MongoDB for Open edX

This guide lists the steps required to successfully and safely roll back MongoDB on an Open edX instance,
in the event that an undesired change is made to course or forum data.

Ticket where we did this: [FAL-1733](https://tasks.opencraft.com/browse/FAL-1733) (internal link)

## Risks

- MongoDB will get out of sync with the RDS.
  This may cause issues if students have interacted with new courses created or content changes made after the time we're rolling back to.


## Roll back MongoDB

First, immediately take a snapshot/backup of MongoDB as it is right now.
This is to ensure that you can return if the rollback needs to be aborted.

Then, we can roll back the MongoDB cluster to the desired snapshot or backup.


### Atlas

In Atlas, this is as simple as clicking "restore" on a snapshot (ref: [MongoDB Atlas: Restore a Cluster from a Cloud Backup](https://docs.atlas.mongodb.com/backup/cloud-backup/restore/)).

As an idea of downtime required: the restore process took 5 minutes for a 3 cluster, 4GB instance.
The cluster is down during the process, since the live data is wiped, and the snapshot data loaded into the cluster.

### Others

Other services may differ depending on the type of backups in place.


## Clean caches and rebuild indexes

Once the MongoDB has restored, most things should be back up.
However, a lot of things are heavily cached and indexed in Open edX, so each one needs rebuilding to be in sync with the new MongoDB.
Expect several broken pages on course content until all the steps below are completed.

SSH into an appserver, and go through the following steps.

Change to the `forum` user and rebuild the Elasticsearch forum index:

```
forum@instance:~/cs_comments_service$ rake search:rebuild_index
```

Rebuild the course blocks cache:

```
(edxapp) edxapp@instance:~/edx-platform$ ./manage.py lms generate_course_blocks --all_courses
```

Reindex the courses.
Delete the index from the Elasticsearch server first, so there aren't orphans left in the index.
Note that Elasticsearch may not be running on localhost. Check `/edx/etc/lms.yml` for the `course_structure_cache` location.

```
(edxapp) edxapp@instance:~/edx-platform$ curl -XDELETE 'http://localhost:9200/courseware_index/courseware_content'
(edxapp) edxapp@instance:~/edx-platform$ ./manage.py cms reindex_course --all
```

Start the LMS Django shell, and run this task for all course ids, to rebuild the `DiscussionsIdMapping` between discussion IDs and discussion XBlock keys.
This can be omitted for any course where discussion XBlocks haven't changed.

```
from lms.djangoapps.discussion import tasks
tasks.update_discussions_map.apply_async(args=({'course_id': 'course-v1:edX+Demo+2021'},))
```

