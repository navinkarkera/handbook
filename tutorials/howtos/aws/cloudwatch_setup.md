# CloudWatch Setup Tutorial

Purpose: [AWS CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html) is a service for monitoring infrastructure in AWS and elsewhere. This tutorial is about setting it up for EC2 and RDS instances.

This will configure email subscriptions through [Amazon Simple Notification Service](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/US_SetupSNS.html) that will push notifications whenever CloudWatch resource monitoring crosses certain thresholds.

## Prerequisites

### AWS Account

If you don't have an AWS account yet, [sign up now](https://aws.amazon.com).

## 1. Setup Amazon Simple Notification Service

Access [AWS SNS Console](https://console.aws.amazon.com/sns/) and click `Create topic` to be used for push notifications. Enter a topic name and description.

![Amazon SNS screenshot](./images/amazon_sns.png)

You'll be redirected to the topic details where you can click `Create subscription` to add a destination for messages in this topic. Make sure to select `Email` on the Protocol dropdown and enter the destination email.

![Topic subscription screenshot](./images/amazon_sns_1.png)

Finally, take note of the `Topic ARN`.

## 2. Setup CloudWatch alarm for EC2 instances

Access [AWS EC2 Console](https://console.aws.amazon.com/ec2/) and select the instance that CloudWatch will monitor.

Select the `Monitoring` tab on instance details and click `Create alarm`.

Select the metric and threshold that the alarm should watch, i.e. *Whenever Average CPU Utilization Is >= 90 Percent* and the number of consecutive periods that is needed to trigger the alarm. Make sure to pick a number high enough that actually shows a problem or the need to spawn more instances.

Also make sure that *Send a notification to [previously created subscription]* is checked.

![Amazon EC2 Alarm screenshot](./images/ec2_alarm.png)

## 3. Setup CloudWatch alarm for RDS instances

Access [AWS RDS Console](https://console.aws.amazon.com/rds/) and select the instance that CloudWatch will monitor.

Scroll down the instance details to find the `Create alarm` button. The interface to create an alarm present the same options described above, including specific metrics for the database instance such as *Failed logins* and *Free Local Storage*.

The workflow is the same, select a metric and threshold, making sure the correct ARN topic is selected to receive notifications.

![Amazon RDS Alarm screenshot](./images/rds_alarm.png)

## 4. View created alarms

The [CloudWatch Console](https://console.aws.amazon.com/cloudwatch/) presents a list of alarms created along with details and history of each alarm.
