# edX AWS Upgrade Tutorial

Purpose: We have previously documented how to set up an instance of Open edX on AWS (see [AWS deployment tutorial (for single VMs)](AWS_deployment_tutorial.md) or the [AWS ELB deployment tutorial (for multi VMs)](AWS_ELB_deployment_tutorial.md)), but the process for upgrading an instance to a newer version of Open edX is different than setting one up from scratch. This document explains how to do the upgrade.

Note: it's worth checking how the initial setup docs may have changed since the previous deployment, as there may be new best practices, services, or variables added.

Upgrading an AWS service
------------------------

Notify the client ahead of time if you're planning a service update or upgrade, to give them time to warn their users
about potential outages, and to make sure they're not surprised in case of unexpected or planned disruption to the
service due to database migrations or other changes to services shared by the old servers.

Check that there are backup snapshots of the RDS and mongo data before beginning the update.

Use the existing security groups and private keys created for previous deployments, and keep the instance sizes and
active VM counts consistent with previous deployments, unless a deliberate change is required.

Preserve a copy of the previously-deployed branches somewhere, in case we need to compare/revert later.

Update the relevant branches and variables in the client's secure repo, and ensure that the director instance has the
correct versions of the secure and `configuration` repos checked out before proceeding.

Run `make requirements` in the `configuration` repo to update dependencies before running the playbooks.

See the `README.md` file on the client's secure repo for instructions on which playbooks to run for that client.

Be sure to update the `hosts` file in the client's secure repo before deploying [additional monitoring and logstash
services](AWS_deployment_tutorial.md#additional-services) to the new VMs.

Activating the new server
-------------------------

For single VM deployments, reassign the Elastic IP to change which server receives requests.

For multiple VM deployments, change the Instances listed in the Elastic Load Balancer to activate the new servers.
Take care when removing instances from the ELB, as it takes a minute or so for new instances to come online.  So to
avoid outages, ensure there is at least one active instance left in the ELB at all times.

Decommissioning the old server
-----------------------------

Once you've deployed the new service, it's time to decomission the old server(s).

Shell into the old server(s) and run:

```bash
# Ensure the tracking logs are rotated to S3
sudo logrotate -f /etc/logrotate.d/hourly/tracking.log

# Remove the instance from the consul cluster to stop our monitoring
sudo consul leave
```

Now, it's safe to stop the old servers.

We generally keep one set of old servers in case a rollback is required. But to reduce the client's costs, terminate any
previously stopped servers.
