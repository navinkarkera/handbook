# edX AWS Multi-Instance Deployment Tutorial

Purpose: This tutorial will walk you through deploying edX to [Amazon Web Services](http://aws.amazon.com). We will deploy two types of edX instances - an instance that will host the RabbitMQ service (`edx-services`) and two instances that will run the rest of edX software, including the django applications that serve the LMS and the Studio (`edx-app`).

The reason for the split is that `edx-services` instance persists state (RabbitMQ queues) and can't be terminated/swapped at will, while `edx-app` instances are stateless and can easily be shut down and replaced.

We will be using [Ansible](http://ansible.cc) to automate deployment.
We will be using external MySQL ([RDS](http://aws.amazon.com/rds)) and
MongoDB ([MongoLab](http://mongolab.com)) databases in order to
provide better scalability and fault tolerance.

<br><br><br>

--------------

**TODO**: These instructions need to be replaced with Terraform code templates. All new AWS infrastructure should be defined and managed by Terraform.

Do not manually provision any new servers using the steps below! Instead please do it using Terraform and then replace these instructions with the new Terraform-based instructions. If you don't have time/budget to fully use Terraform, then please at least partially use Terraform and make sure each new deployment is using more Terraform than the last. See [the Terraformed Infrastructure epic](https://tasks.opencraft.com/browse/SE-2436) for details.

There is already a guide for AWS Resources Setup using [terraform here](AWS_terraform_deployment_tutorial.md), 
which should replace the resources setup sections only, for installation (ansible scripts, etc.)
you should still follow this guide.

--------------

<br><br><br>


Prerequisites
=============

AWS Account
-----------

If you don't have an AWS account yet, [sign up now](http://aws.amazon.com).

MongoDB Atlas Account
---------------------

You will also need a [MongoDB Atlas account](https://www.mongodb.com/cloud).
There is a shared OpenCraft Account that can be used when setting up client
instances, you can find the credentials for that in the vault [here](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20MongoDB%20Atlas).

AWS Resources
=============

We will create two EC2 instances, an RDS instance, two S3 buckets, an
ElastiCache cluster, and an Elastic Load Balancer on Amazon Web
Services. We will be using instance types and settings that are a good
starting point for mid-traffic edX installation. Depending on your use
case, you may need to tweak the values to better suit your specific
needs.

EC2 edx-app Instance
--------------------

We will be using an [EC2](http://aws.amazon.com/ec2) instance to
host the django and ruby edX applications (LMS, CMS, and the forums).

Follow the
[instructions from our standard AWS installation docs](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#ec2-instance)
to create the `edx-app` instance.

EC2 edx-services Instance
-------------------------

Now create a separate instance, which will host the RabbitMQ
service. Follow the same steps to create the `edx-services`
instance. The only difference is that you should only assign the
`default` security group to it, and not create any new security
groups.

RDS Instance
------------

We will be using an [RDS](https://aws.amazon.com/rds) instance to
host the MySQL database used by the main django applications.

Follow the
[instructions from our standard AWS installation docs](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#rds-instance)
to create a new RDS instance.

S3 buckets
----------

Follow the
[instructions from our standard AWS installation docs](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#s3-buckets)
to create the required S3 buckets.

Elastic Load Balancer
---------------------

We will use an Elastic Load Balancer (ELB) to distribute the traffic
among multiple `edx-app` instances.

Select the *Load Balancers* tab from the EC2 Dashboard. Click the
*Create Load Balancer* button. On the next step, select *Application
Load Balancer*. Give it a recognizable name, such as
`edxapp-load-balancer`.

Under the *Listeners* section, HTTP protocol over port 80 is enabled
by default. Click *Add listener* to add another listener for *HTTPS
(Secure HTTP)* (port `443`).

Under the *Availability Zones* secton, select the VPC where you
created the EC2 instances (this will probably be the Default
VPC). Select all availability zones.

In the *Configure Security Settings* step step, configure your SSL
certificate.

In the next step, select *Create a new security group*. Rules that
make ports `80` and `443` accessible from `Anywhere` should already be
predefined by default. Add them if they are not already there.

On the *Configure Routing* step, create a new target group. Give it a
recognizable name, set *Protocol* to `HTTP` and *Port* to
`80`. *Target type* should be set to `instance`. Under *Health
checks*, set *Protocol* to `HTTP` and *Path* to `/heartbeat`.

In the *Register Targets* step, select the `edx-app` instance you
created two sections above and click *Add to registered*.

Go to *Review* and click *Create*.

Now go to the *Target Groups* section and select the target group that
was automatically created with the load balancer. Select *Actions ->
Edit attributes*. Click the checkbox to enable *Stickiness* and set
*Stickiness duration* to `180` seconds. Some parts of edx-platform do
not function correctly without sticky sessions.

ElastiCache Cluster
-------------------

### Memcached

We will be using [ElastiCache](http://aws.amazon.com/elasticache/)
as a scalable replacement for memcached.

Navigate to the ElastiCache Dashboard from the AWS console, then
switch to the *Memcached* section. Click the *Create* button. Under
*Cluster engine* select *Memcached*. Give the cluster a recognizable
name, and select the `cache.m3.medium` *Node Type* and enter `1` in
the *Number of Nodes* field.

Make sure the `default` security group is selected. Leave other values
at their defaults and click *Launch Cache Cluster*.

### Redis

We will also be using the Redis variant of
[ElastiCache](http://aws.amazon.com/elasticache/).

Navigate to the ElastiCache Dashboard and switch to the *Redis*
section. Click the *Create* button. Select the `cache.m3.medium` *Node
Type* and set *Number of replicas* to `1`.

Under the *Backup* section, disable *Enable automatic backups* since
Redis is used merely as a queue in edx-platform, so backups are not
really useful.

Make sure the `default` security group is selected. Leave other values
at their defaults and click *Launch Cache Cluster*.

Elasticsearch Domain
--------------------

We will now set up a new
[Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/)
domain.

Navigate to the Elasticsearch Service dashboard from the AWS
console. Click the *Create new domain* button. Give it a recognizable
name and select `1.5` under *Elasticsearch version*.

On the next step, select the `m3.medium.elasticsearch` instance
type. Click the *Next* button. On the next step, select `VPC
access`. Under *VPC* select the default VPC. Under *Subnet*, pick one
of the available subnets (it does not matter much which one you
pick). Make sure `default` security group is selected under *Security
groups*. Under *Access policy*, select *Do not require signing request
with IAM credential*. Click *Next*.

Review the settings and if everything looks correct, click *Confirm*.

MongoDB Atlas Database
----------------------

Follow the [instructions from our standard AWS installation docs](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#mongodb-atlas-databases)
to create two Mongo databases (edxapp and edxforum).

Ansible Playbook
================

We will be basing our deployment on the `edx_sandbox.yml` playbook
from the edX [configuration](https://github.com/edx/configuration)
repository, so clone the repository:

    git clone git@github.com:edx/configuration.git

Install the required python packages. You will probably want to create
a dedicated python virtualenv, but that's out of the scope of this
tutorial:

    cd configuration
    pip install -r pre-requirements.txt
    pip install -r requirements.txt

This will install ansible among other dependencies.

We will split the `edx_sandbox.yml` playbook into two parts - the
`edx_services.yml` playbook that will deploy the services and the
`edx_app.yml` playbook that will deploy the app instances. Check
[gsehub_services.yml](https://github.com/gsehub/configuration/blob/gsehub/playbooks/gsehub_services.yml)
and [gsehub_app.yml](https://github.com/gsehub/configuration/blob/gsehub/playbooks/gsehub_app.yml)
for an example.

Sensitive Data
--------------

Follow the
[instructions from our standard AWS installation docs](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#sensitive-data)
to create a private git repository containing sensitive data, such as
database passwords.

We will also be adding the `hosts.lst` file to our private
repository. The `hosts.lst` file is the ansible
[inventory file](http://docs.ansible.com/intro_inventory.html). We
will split it into two sections - one for the `edx-app` instance and
one for the `edx-services` instance:

    [app]
    1.1.1.1  # Replace this with your edx-app insatnce IP address
    [services]
    1.1.1.2  # Replace this with your edx-services IP address.

Remember to set the `EDXAPP_RABBIT_HOSTNAME` to the internal IP of the
`edx-services` instance in your `vars.yml` file.

Running the Playbook
--------------------

To run the playbooks, go to the `playbooks` folder inside the
`configuration` repository. It is important to run the playbook from
inside the `playbooks` folder, so that extra ansible configuration
in `playbooks/ansible.cfg` that the edX playbooks depend on gets
picked up by ansible:

    cd configuration/playbooks

First, run the playbook to deploy and set up the `edx-services` instance:

    ansible-playbook -i /home/edx/edx-secure-config/hosts.lst \
                     -e @/home/edx/edx-secure-config/vars.yml \
                     -u ubuntu \
                     --private-key=/home/edx/edx-secure-config/cert.pem \
                     edx_services.yml

Then deploy the `edx-app` instance:

    ansible-playbook -i /home/edx/edx-secure-config/hosts.lst \
                     -e @/home/edx/edx-secure-config/vars.yml \
                     -u ubuntu \
                     --private-key=/home/edx/edx-secure-config/cert.pem \
                     edx_app.yml

Once the playbook finishes running (it can take a long time!), your
edx-services instance should be all set up and running.

Creating an AMI
---------------

Verify that the `edx-app` instance is running correctly by visiting
its public IP. If everything is working fine, create a new image (AMI)
from it. To do this, go to the EC2 instances dashboard and select the
`edx-app` instance. Under *Actions -> Image* click *Create Image*.

Image creation can take some time. You should see the new image under
the *AMIs* tab in the EC2 Dashboard once the image has been created.

Launch another edx-app instance
-------------------------------

We can now easily create another edx-app instance from the AMI.

Select the AMI and click *Actions -> Launch*. Follow the steps from
the *EC2 edx-app Instance* section to set up the settings for the new
instance. Once the instance launches, the edX application should be
installed on the instance, configured, and running.

We will now add the instance to the ELB. Go to the *Target Groups*
tab, select the target group associated to your load balancer, and
switch to the *Targets* tab and click the *Edit* button. From the list
of available instances select the new `edx-app` instance that we
created from the AMI, and click *Add to registered*, then click
*Save*. Once the health check successfully completes, the instance
status should change from *initial* to *healthy* and the ELB will
transparently balance the load between the two `edx-app` instances.

We could add more `edx-app` instances following the same steps if
needed.

Verification & Monitoring
=========================

Follow the instructions from our standard AWS installation docs to
[verify the instance](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#verifying-the-instance),
[deploy OpenCraft's additional services](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#additional-services),
and to [set up monitoring](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/aws/AWS_deployment_tutorial.md#new-relic-monitoring).

CloudWatch monitoring
=====================

AWS provides EC2 and RDS monitoring along with alarms that send notifications whenever a certain preconfigured threshold on resources is reached. See [CloudWatch Setup](cloudwatch_setup.md) for instructions to set it up.

TODOs
-----

* Set up SSL on the ELB using AWS Certificate Manager
