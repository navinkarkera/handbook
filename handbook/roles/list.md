# Roles and Expectations

One other important way that we do differently than a lot of companies is the way we assign
responsibility. Instead of titles, which are linked to the concept of hierarchies and ranking,
we use [roles](#roles), and all people in the organization share a
[set of expectations](#what-is-expected-from-everyone).

## What is expected from everyone

The general expectations for anyone working at OpenCraft:

* I can work from wherever I want, provided I have access to a reliable high-bandwidth internet
  connection good enough for video calls.
* I can set my own work schedule, and work when I want during the week, as long as **I remain
  available to answer to others** (please see: the **Communication** section below)
* (In general) **I am *not* expected to work weekends** (unless I'm behind on my commitments),
  which should remain an exception, not the rule! If you find yourself forced to work on week ends
  more than once per month, that likely reflects an issue with your time management that needs
  fixing. If I work week ends as a personal choice, I will not expect other team members to also
  work week ends.
* I will make sure my [cell's responsibilities](#cell-member) are continuously being properly
  taken care of, by reviewing their status at least once per week.
* I will work at least the amount of hours per week that is specified in my contract (e.g. 40
  hours/week), averaged over each month, excluding scheduled holiday time.
* I will ensure that clients and people on the cell that I'll be working with (e.g. code reviews)
  know my availability. (e.g. If I'm taking Friday off, I'll make sure people who need me
  to do code review know that).
* When taking time off I will follow the procedure described on the [vacation section](../processes/vacation.md).
* If I'm unexpectedly sick or unavailable due to an emergency, I'll make every effort to notify
  my cell and ask the [sprint firefighters](#firefighter) to take over my duties.
* I will provide my own hardware (laptop).
* I will keep my computer(s) secure and up to date as described in the
  [security policy](../security_policy.md).
* I will record all hours that I spend on a task using the Tempo timesheets tool in JIRA
  (including things like setting up devstacks and time spent learning, which we all need to do
  and is an important part of the task, especially at the beginning, or when joining a project).
* If I use another tool for tracking my time,
  I will update the JIRA Tempo timesheets within 24 hours (excluding weekend days).
* I will attend the Open edX Conference every year, unless exceptional and important personal
  circumstances prevent me from being present (Note that team members who joined OpenCraft before
  July 31st 2018 are [only encouraged to attend the conference, but not strictly required to](https://forum.opencraft.com/t/open-edx-conference-attendance/134/20)).
* As a conference attendee, I will submit a talk proposal and present it. If my talk isn't
  accepted, I will co-present a talk with someone else from OpenCraft.

### Communication

* **If any client email is addressed to me, I will respond to it within 24 hours (excluding weekend
  days), or ensure that someone else on my cell does.**
    * I will CC or forward to help@opencraft.com all client emails and replies, so that others can take over the thread
      with that client if needed, or refer to it for context.
    * If I can't answer immediately, I will at least send a quick reply to let them know we're
      working on a response, and when they should expect it.
* **I will reply to pings/emails from other OpenCraft team members within 24 hours**
  (again excluding weekend days and scheduled time off).
    * But this should be a "worst case" scenario - completing the work on time
      is still the primary goal, so when someone is blocked and pings me for help,
      I will try to do as much as I can to unblock them quickly,
      rather than starting a 24h ping-pong cycle that takes up all the time
      in the sprint without accomplishing any work.
* **I will respond to pings on GitHub or GitLab within 24 hours**
  (with the usual exclusion for weekend days and scheduled time off).
    * If a ping has no corresponding ticket, or the ticket is not scheduled for
      the current sprint, I will respond to the ping with an estimate for when
      they can expect a full response.
* I will read and respond to forum threads:
    * On the [announcements forum](https://forum.opencraft.com/c/announcements/7): within 24h, excluding week-ends & time off;
    * On the rest of the forums, within 2 weeks, except for the [off topic forum](https://forum.opencraft.com/c/off-topic/8), which doesn't need to be read or replied to at all.
* I will be professional and polite when communicating with clients.
* I will prefer asynchronous over synchronous processes (keep meetings to a minimum). A chat
  conversation is a form of a meeting.
    * Generally, discussions should happen first asynchronously on the JIRA tickets;
      if there is really something that can't be efficiently sorted out asynchronously,
      have a chat or schedule a meeting. JIRA might have long response cycles (around
      1 day turnaround). If this time isn't enough to unblock someone and
      finish their sprint commitments, use Mattermost, even though
      it's more disruptive to people's workflow.
    * If you do have a synchronous conversation with someone about a particular task,
      post a summary of the result/decision from that conversation on the JIRA ticket
      for easier future reference.
    * When scheduling meetings, give them a precise agenda. For people using Calendly,
      like Braden and Xavier, book meetings there, as it allows us to avoid
      the scheduling overhead.
    * Try, as much as possible, to use a similar approach with clients - don't
      let them invade your days with meetings. Calendly is good for this too,
      as it allows to define time slots where you'll have the meetings,
      to minimize the disruption they cause to your day and productivity.
      If you want a calendly account, let Xavier know and he will set
      you up with the OpenCraft account.
* I will post on public channels on Mattermost rather than private 1-to-1 channels
  whenever possible, even if the message is just for one person.
  This allows us to know what others are working on, and helps to replace
  the overheard discussions in physical offices - it can also be an occasion
  for someone else with knowledge about your issue to get the context,
  and to intervene if it is useful to the conversation.
* I will make sure I communicate with my reviewer(s) on tasks about availability
  and timezone overlap if I didn't have knowledge about it before. I will use the
  [contact sheet](https://docs.google.com/spreadsheets/d/107dR9H1vWjLpJlXPuBaFJIFaEPEmaSh50xLow_aEGVw/edit)
  where necessary.

### Sprint tasks

* I will follow the process and expectations outlined in the [pull request process](../processes/pull_request.md)
  section.
* I will never add my code (even DevOps code!) to a production branch directly - I will always
  create a Pull Request.
* I will always ensure my Pull Requests are reviewed by another OpenCraft team member **before** merging,
  except if I am a *core team member* and I'm merging *trivial changes*.
  In this exceptional case, I may merge the *trivial changes* before receiving a review,
  but I will then ensure all of those *trivial changes* are reviewed and acknowledged post-merge or
  post-deployment by another OpenCraft team member. Trivial changes include:
    * Small Open edX environment tweaks ([example](https://user-images.githubusercontent.com/514483/34581710-c167d10c-f191-11e7-9a66-7fa8c59def82.png))
    * Minor/bugfix package version bumps ([example](https://github.com/open-craft/ansible-rabbitmq/pull/4/files))
* I will only commit to work each sprint that I believe I can comfortably complete within the
  allotted sprint time (two weeks). Here, "complete" means "get to external review or get merged, deployed, and delivered."
* As a core team member, I will avoid taking [newcomer-friendly tasks](../task_workflows.md#newcomer-friendly-tasks) unless they are urgent
  and there are no newcomers available to take them on.  I will take on reviews of newcomer-friendly tasks, and allow
  time to provide mentoring and extra guidance.
* I will get all of my tasks ready for internal review (by someone else on the OpenCraft team) by the
  **end of Wednesday in the second week of the sprint** at the latest. This will ensure that
  there is time for the review to take place, and for me to address the review comments
  and get the internal reviewer's approval in time.  If a ticket potentially requires multiple
  review cycles, get it into review as early as possible.  Schedule reviews with your reviewer
  to make sure they have time when you get your work ready for review.
* I will spend time to plan each task at the beginning of each sprint, including scheduling the
  review with the reviewer. Make sure you have an answer for:
    1. When do each step of the task need to be completed?
    1. Which days will you work on which task?
    1. When do individual parts need to be completed to be on time?
* It's also important to keep some margin on the sprint, in case something doesn't go as expected.
* I will get my tasks to at least **External Review** (or "Deployed & Delivered"
  if no external review is required) by **one hour before the next sprint**. As
  this is also dependent on the internal reviewer, I'll make sure she/he will be
  available around the time I finish.
* If it is looking like I will have trouble meeting one of these deadlines,
  I will inform the [sprint firefighters](#firefighter) (and [epic owner](#epic-owner)) as early as possible,
  so they can get me additional help, start planning for contingencies,
  and/or warn the client.
* If necessary, I will work overtime to complete my tasks before the end of the sprint.
* I will prioritise stories that spilled over from the previous sprint.
* I will "work from the right", prioritizing responding to external reviews
  as the highest priority, then doing reviews / responding to questions
  from others on the team, and finally working on implementing my own "In Progress" tasks.
* I will be helpful to team members, responding to questions,
  helping with debugging, providing advice, or even taking over tasks
  if I have time and they are overloaded. This has precedence over starting
  to work on new tasks, when finishing a sprint early.
* Once a sprint, I will review all of my tasks that are in "Long External Review/Blocked"
  and if needed, I will ping the external reviewer/blocker or pull the task into
  an upcoming sprint.

## Roles

Roles can be taken on by willing people, and people will usually take on multiple
roles, changing over time, based on interest and availability. This allows for a much smoother
progression of responsibilities than the ladder climbing game.

Note that responsibility is also uncorrelated with compensation and raises, which are given the
whole core team at once, based on time spent working at the company and overall financial results.
Compensation isn't a good source of motivation beyond a certain level, and this approach removes
a lot of politics.

If there is any role you would like to take up, the best way is to say it publicly - for example
in the forum. Then when opportunities arise others will remember it, and point you to tickets
you can own. Keep in mind that role assignments are intended to be permanent, so keep that in
mind before picking a role. Ideally you should work in a role for at least one year before you
consider swapping/dropping it.

### Changing Roles

Once someone takes up a role in a call there generally no need to reassign it unless someone leaves
OpenCraft. While an appointment to a role is generally permanent, no one should feel stuck in a
role if they are no longer comfortable in it. As such it is desirable to openly discuss with other
members of the cell on the forum.

It is the responsibility of the current person with a role to find a replacement in their cell and
help their replacement during a transition period while they are still getting comfortable with
their new responsibilities.

Note that for roles like Client Owner or DevOps Specialist where the role involves a fair bit of
specialised knowledge or context, great care should be taken to find a suitable replacement with
a similar level of knowledge and context.

### Types of OpenCraft members

There are three types of members at OpenCraft:

* *Additional members*:
    * *Short-term members* (temporary contractors), who have been hired for a specific task, scope
      or period of time. They are the most external members of the team.
    * *[Newcomers](#newcomer) on probation* (2 months, renewable).
* *Core team member* - the new recruits who have been confirmed become core
  team members. They differ from the other types of members in that they tend
  to have more team-based responsibilities.  For example, core team members are
  on a [weekly rotation
  schedule](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit#gid=447257869)
  where they often have to take on some additional roles, including Sprint
  Firefighter and being on Discovery Duty.

However, in all other regards, all types of developers are put to the same expectations -- no
politics or special treatment between short-term developers, newcomers, and core team members.

The only acceptable exception is when providing extra mentoring on tasks for newcomers (or anyone
known to not have much context in the task), which is expected and useful.

### Recruitment manager

The recruitment manager role is responsible for organizing the selection of candidate hires
to match the needs of the [epic planning spreadsheet] for the cell, as determined by the
[epic planning manager](#epic-planning-and-sustainability-manager). This role is intended
as a support role that would provide recruitment work for all the other cells. This includes:

* Selection of newcomers (see the [recruitment checklist](https://opencraft.monday.com/boards/948961077/)):
  * Pre-selection of candidates for interviews among the received candidatures;
  * Interviews (scripted, recorded for asynchronous review);
  * Selection of newcomers among the interviewed candidates (with review & confirmation by CEO);
  * Correspondance with candidates (positive & negative answers), except contract negotiation handled by the CEO;
* Onboarding and offboarding of newcomers, including:
  * Starting the onboarding process;
  * Creation of their accounts on the OpenCraft tools & granting rights;
  * Creation of their onboarding epic and tasks;
  * Ensure the newcomers have good conditions to work: ahead of a newcomer start date and before assigning them to a cell, ensure the cell has a mentor assigned and can guarantee a good line up of tasks.
  * And after, ensure the trial goes well, implement changes like the trial projects, etc.
  * The role would be responsible, overall, for increasing the number of newcomers who become core team members
* Organization of the review and confirmation of core developers:
  * Ensure that all core members of the cell participate in the review, as well as one core member from each other cell;
  * Compile the feedback from individual reviews in a consolidated list, which doesn't include the names of individual reviewers;
* Regularly review recruitment trial results for interview selection accuracy.

We will have a single backup for the recruitment manager (in cases of unavailability).

### Cell management

The coordination on each of the cell management roles listed below is to be assigned
to an individual member of the cell as a recurring epic. However, the way that each role
is handled beyond what is described in this handbook is the responsibility of the cell as a whole,
with all of its members being considered as weekly reviewers:

If desired, each role can be handled via multiple tasks assigned to different owners,
as long as the overall responsibility for coordinating the work involved in a specific role
is assigned to a single person.

Unless otherwise stated, all of these roles should exist on any type of cell:

#### Sprint Planning Manager

This role is different from the [Sprint Manager](#sprint-manager), though both roles can be taken by the same person if desired.

The planning steps and their schedule are described in more detail in [Sprint Planning Process](../processes/sprints.md#sprint-planning-process), but here is an overview:

* **Task Estimation Session**: Each sprint an estimation session for tasks that are scheduled for the following sprint
  is created automatically in JIRA. Towards the end of the sprint the estimation session is closed and story points
  are assigned to tickets based on estimates that team members provided. This step also happens automatically.
  The only manual step that the Sprint Planning Manager needs to complete is to *add epic owners as Scrum Masters
  to the estimation session* so that they can update the session with any tickets that did not exist prior to the initial launch
  of the session, and that need to be included in the upcoming sprint.
* **Prioritization**: Each cell is closest to the needs of its clients and knows best the work that needs to be performed
  in the coming weeks or months, so it's in the best position to prioritize its own backlog accordingly.
  As part of this step, tasks should be arranged in the following order (decreasing priority):
    1. Client work to ensure that the clients' needs are met, and that they are happy with
       our work.
    1. [Newcomer-friendly tasks](../task_workflows.md#newcomer-friendly-tasks) should be ranked high enough in the list of tasks to
       allocate time for core team member review/mentoring. The earlier we can get newcomers up to speed, the better
       the workload will be for everyone.
    1. Internal projects flagged by the management as priorities.
    1. Additional work the cell finds useful to its or OpenCraft's function (or the Open
       edX community's), as long as the cell remains sustainable in proportion of
       billable hours. The cell defines and prioritizes the additional internal work,
       without a specific monthly budget cap. (Epic budgets remain a good practice to
       follow, but the amount of internal work scheduled for a given sprint doesn't require approval from management.)
* **Task Insertion**: See [Sprint Planning Process](../processes/sprints.md#sprint-planning-process) > [Task insertion](../processes/sprints.md#task-insertion) for details.
* **Sprint Preparation**
    * Make sure that a social chat has been scheduled for your cell.
    * Make sure that your cell and its members aren't overcommitted, and work with epic owners to push tasks out to *Stretch Goals* if necessary.
    * Make sure that tickets for next sprint are green (i.e., that they have an estimate, an assignee, and a reviewer). Follow up with team members on tickets that aren't green, and push unassigned tickets out to *Stretch Goals*.
    * Lastly, complete the current sprint and start the new one.

Note: It's important to keep in mind that to remain capable of leading initiatives at the company
level, not just at the cell level, hierarchy can retain an important role in prioritization.
Anyone in a cell can propose or prioritize a company-wide initiative, provided they get approval
and buy-in from people it would affect. It would escalate to management if there's a conflict or
disagreement about that, for example about how priorities relate to each other.

#### Sprint Manager

Each cell has its own sprint board and tickets, and is responsible for managing the sprint,
including necessary preparations and wrapping up completed sprints.

* **Sprint Preparation**: Every Monday before the start of the new sprint:
    * Double-check if any of the people assigned to a rotation in the upcoming
      sprint are off, and if so that a backup will be available (e.g. that the second [firefighter](#firefighter)
      will be present, or that a third firefighter has been assigned on those days).
* **Assigning Rotations**. The Sprint Manager should keep the [Weekly Rotations Schedule](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit#gid=1068304496) up-to-date.
    * This can happen at any time during the sprint, as long as the spreadsheet is checked at least once per sprint, and updated as necessary.
    * How to assign rotation hours within the cell is up to each cell,
      as long as the number of hours matches a % of the total hours of work in the cell. The hours
      can be adjusted slightly if more or less firefighting is required:
        * Firefighting budget: 7.5% (30h/week for 10 full times, split between two firefighters each sprint)
        * Discovery budget: 2.5% (10h/week for 10 full times). The discovery budget and the discovery duty allocation are two different things.
          The weekly discovery duty allocation (5h/cell/week) is to be used for new tasks that pop-up over the course of a sprint - it is funded by the discovery budget.
          Any leftover discovery budget can be used for discovery tasks that are planned in advance.
* **Mid-sprint updates**:
    * Confirm that all members of your cell have posted a mid-sprint update (text only), and follow up with people as necessary via your cell's *Sprint Updates* thread until everyone's posts are available.
* **Sprint Wrap-Up**:
    * Confirm that the [spillovers spreadsheet](https://docs.google.com/spreadsheets/d/1aJD-e2NkDsyBq_yBHykGiMYE29FvSOuYSCZjOJ5sjkM/edit#gid=0&fvid=787610946) for your cell
      contains explanations for all spillovers listed for the previous sprint. It is updated by [SprintCraft] at the end of each sprint.
    * Create the Sprint Retrospective forum post/poll and create new entries in the [retrospective spreadsheet](https://docs.google.com/spreadsheets/d/16zyBstwMrcNJXELUB4DmCeG9IVACrZEC3M3SHoEtvq0/edit) for the new sprint.
    * Confirm that all members of your cell have posted an end-of-sprint video update, and follow up with people as necessary via your cell's *Sprint Updates* thread until everyone's posts are available.
    * Review sprint deliveries: Move tasks from *Deployed & Delivered* to *Done*, after double-checking that all [the criteria for calling it "Done"](../task_workflows.md#done) have been met.
* **Meetings**: For cells, such as the business cell, which don't have an asynchronous planning session, the Sprint Manager leads the sprint planning meeting.

#### Epic Planning and Sustainability Manager

See [Processes > Epic Planning and Sustainability Management](../processes/epic_planning_and_sustainability_management.md)
for additional information about the responsibilities listed below.

##### Epic Planning

*Every sprint*:

* Review and evaluate status changes of individual epics from the past sprint.
    * Make sure that:
        * Each epic that is *In Development* has an epic update.
        * The Epics board correctly reflects the status of each epic.
        * The ["Time / Estimates"](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1)
          section of the epic planning spreadsheet is up-to-date with info on new and completed epics.
    * In case of any doubt about the correct status for an epic leave a comment on the epic.
* Compare the Epics board for your cell with the [epic planning spreadsheet] and update the spreadsheet as necessary.
* Add/Update team member availability in the [epic planning spreadsheet].
* Handle requests from cell members to change their weekly hour commitments.
* Record information about upcoming vacations in the [epic planning spreadsheet].
* Adjust capacity allocations for coming months as necessary.
* Ensure delivery and bugfix deadlines for individual epics are on target.
* Coordinate with the team's recruitment manager to slow down/speed up recruitment as necessary.
* Post an update on the "Epic planning and sustainability management - &lt;your cell&gt;" epic
  with the following checklist:
```text
h3. Epic planning and sustainability update (Sprint ???)

h4. Epic status

* ( ) Review and update epic statuses as needed.
** ( ) New epics ("Prospect", "Offer / Waiting on client", "Accepted"):
*** ( ) Add an entry to the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.
*** ( ) Move to "In Development" if work has started.
** ( ) In development:
*** ( ) Make sure epic updates are posted by epic owner/reviewer.
*** ( ) Move epics that have become blocked on the client to "Offer / Waiting on client".
** ( ) Delivered epics:
*** ( ) Move to "Delivered to client / QA".
** ( ) Completed epics:
*** ( ) Move to "Done".
*** ( ) Update corresponding entries on the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.

* ( ) Compare the Epics board with the epic planning spreadsheet and update the spreadsheet as necessary.

h4. Availability updates (recruitment, weekly commitments, vacations)

* ( ) Follow up on pings from the team's recruitment manager:
** ( ) For people joining the team, add availability and onboarding hours for the coming months to the epic planning spreadsheet.
** ( ) For people leaving the team:
*** ( ) Update their availability for the coming months.
*** ( ) Help find new owners for their responsibilities (roles, clients, epics).

* ( ) Handle requests from cell members to change their weekly hour commitments.

* ( ) Check the calendar for vacations coming up in the next couple months. If someone will be away for a total of 1 week (or longer), [post a comment mentioning their availability|https://gitlab.com/opencraft/documentation/public/merge_requests/99#note_198626533] in the epic planning spreadsheet.

h4. Capacity requirements

_For in-progress epics:_

* ( ) Evaluate the amount of time required to complete the accepted project scope over the next months and update capacity allocations in the epic planning spreadsheet.
* ( ) Ensure delivery and bugfix deadlines of individual epics are on target (or are being actively discussed on the epics). If that's not the case, comment directly on the epic planning spreadsheet, pinging epic owners as necessary.
* ( ) Ensure the projects from your cell's clients are being properly delivered using the epic management process.
* ( ) Coordinate with the team's recruitment manager to slow down/speed up recruitment as necessary.

h4. Client updates

* ( ) Check the descriptions of the Hosted Sites Maintenance and Small Projects & Customizations epics (SE-1690, SE-1693) and update the list of clients for your cell, following the existing format.
** ( ) Add info about new clients that we recently on-boarded. (Make sure to skip clients that haven't moved past the prospect stage.)
** ( ) Remove info about clients that we no longer work with. (Make sure they have been off-boarded completely before doing this.)

h4. Budgeting

_Once per sprint:_

* ( ) *Monday of W3:* Review amount of billable and non-billable work scheduled for next sprint, and post update on your cell's epic planning and sustainability epic.

_Once per month:_

* ( ) *Before the 5th of the month:* Check that tasks from previous month use correct accounts, and post update on your cell's epic planning and sustainability epic.
* ( ) Add actuals for previous month to epic planning spreadsheet.
* ( ) Adjust client budgets for sustainability dashboard as necessary.

_Once per quarter:_

* ( ) Post update about cell's sustainability on forum.
* ( ) Review and update budgets for non-billable cell accounts.

h4. Notes

...
```

*Every month:*

* Add actuals for the previous month to the epic planning spreadsheet.
* Adjust client budgets in [SprintCraft] as necessary to keep the sustainability dashboard
  for your cell up-to-date.

##### Sustainability Management

The sustainability manager is responsible for ensuring that the cell keeps the budgets
it is responsible for in order.

*Every sprint:*

* *Monday of W3:* Compile and post sustainability preview for the upcoming sprint,
  using the following template:
   ```text
   h4. Sustainability preview for Sprint <next> (<cell name>):

   * Period selected: Tue <date> (beginning of Sprint <current>) -- Mon <date> (end of Sprint <next>)
   * Total number of billable hours estimated: h (left this sprint) + h (next sprint) = h
   * Total number of non-billable cell hours estimated: h (left this sprint) + h (next sprint) = h
   * Resulting sustainability ratio ({{non-billable / (billable + non-billable)}}): **%, {lower,higher} than last sprint.
   * Notes:
   ** ...
   * Questions:
   ** ...
   ```
* Follow up with epic owners about budget issues as necessary.

*Every month:*

* *Before the 5th of the month:* Check that tasks worked on over the course of the previous month
  use correct accounts, and post an update on your cell's epic planning and sustainability epic.
  You can use the following template:
   ```text
   h3. Accounts review for <month> <year> (<cell name>):

   h4. Client accounts

   h5. Updates applied

   * <ticket>: Changed account from <previous> to <new>.

   h5. Open questions

   * <ticket>: ...

   h4. Internal accounts (non-cell)

   h5. Updates applied

   * <ticket>: Changed account from <previous> to <new>.

   h5. Open questions

   * <ticket>: ...

   h4. Internal accounts (cell)

   h5. Updates applied

   * <ticket>: Changed account from <previous> to <new>.

   h5. Open questions

   * <ticket>: ...

   ---

   CC <Administrative Specialist>
   ```

*Every quarter:*

* *In January/April/July/October:* Compile and post sustainability update on the forum,
  choosing from the templates listed below.
    * If your cell is currently sustainable, the following format works well for the quarterly sustainability update:
      <details>
        <summary>(click to expand)</summary>

        ```markdown
        # <cell name> - {January,April,July,October} <year> ({Q1,Q2,Q3,Q4} <year> update)

        You can log time spent reading and responding to sustainability updates on the sustainability epic of the corresponding cell:

        * [BB-284](https://tasks.opencraft.com/browse/BB-284) for Bebop updates
        * [FAL-49](https://tasks.opencraft.com/browse/FAL-49) for Falcon updates
        * [SE-248](https://tasks.opencraft.com/browse/SE-248) for Serenity updates

        ## Goal

        <cell name> unlocked the <new target>% limit to non-billable work in <month> of <year>, which means that the goal from <next month> <year> forward is keeping the sustainability ratio below <new target>%.

        ## Sustainability Ratio

        ### Entire reference period (<month> <year> -- <month> <year>)

        The sustainability ratio for the period <yyyy-mm-dd> -- <yyyy-mm-dd> is **<percent>%**.

        ### {Q1,Q2,Q3,Q4} only (<month> <year> -- <month> <year>)

        The sustainability ratio for the period <yyyy-mm-dd> -- <yyyy-mm-dd> is **<percent>%**.

        ## Billable Hours ({Q1,Q2,Q3,Q4})

        The largest source of our billable hours is <client>.

        Top contributors of billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ## Non-Billable Hours ({Q1,Q2,Q3,Q4})

        Top contributors of non-billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ## Projections for (the rest of) <year>

        ...
        ```
      </details>

    * If your cell is currently above its sustainability target, you'll probably want to
      go into a bit more detail with your review and use the following format for the quarterly sustainability update:
      <details>
        <summary>(click to expand)</summary>

        ```markdown
        # <cell name> - {January,April,July,October} <year> ({Q1,Q2,Q3,Q4} <year> update)

        You can log time spent reading and responding to sustainability updates on the sustainability epic of the corresponding cell:

        * [BB-284](https://tasks.opencraft.com/browse/BB-284) for Bebop updates
        * [FAL-49](https://tasks.opencraft.com/browse/FAL-49) for Falcon updates
        * [SE-248](https://tasks.opencraft.com/browse/SE-248) for Serenity updates

        ## Previous updates

        To see where <cell name>'s budgets stood at the end of {Q1,Q2,Q3,Q4}, see the latest sustainability report from [<month> <year>](<forum link>).

        At that time <cell name> had a sustainability ratio of **<percent>%** for the <start year>-<current year> period.

        ## Goal

        <cell name> has yet to unlock the <new target>% limit to non-billable work, which means that <cell name> must continue to target a sustainability ratio of **<current target>%** in the coming months.

        ## {Q1,Q2,Q3,Q4} <year> Sustainability Ratio

        At the end of **{Q1,Q2,Q3,Q4} <year>**, <cell name>'s sustainability ratio for the entire reference period (<yyyy-mm-dd> to <last day of quarter under review>) is **<percent>%** (an {increase,decrease} of <percent>% since last update).

        Detailed breakdown:

        ### Sustainability numbers from *<day> <month> <year> to <last day of quarter under review>* (entire reference period)

        | Total hours | Non-cell hours | Billable cell hours | Non-billable cell hours | % non-billable cell hours | Remaining non-billable hours |
        |---|---|---|---|---|---|
        | <hours> | <hours> | <hours> | <hours> | **<percent>%** (max <current target>%) | **-<hours>** |

        ### Sustainability numbers from *1 Jan <year> to <last day of quarter under review>* (Q1-{Q1,Q2,Q3,Q4} of <current year>)

        | Total hours | Non-cell hours | Billable cell hours | Non-billable cell hours | % non-billable cell hours | Remaining non-billable hours |
        |---|---|---|---|---|---|
        | <hours> | <hours> | <hours> | <hours> | **<percent>%** (max <current target>%) | **-<hours>** |

        ### Sustainability numbers from *1 Sep 2021 to 31 Dec 2021* (Q4 only)

        | Total hours | Non-cell hours | Billable cell hours | Non-billable cell hours | % non-billable cell hours | Remaining non-billable hours |
        |---|---|---|---|---|---|
        | <hours> | <hours> | <hours> | <hours> | **<percent>%** (max <current target>%) | **-<hours>** |

        ## Billable Hours ({Q1,Q2,Q3,Q4})

        The largest source of our billable hours is <client>.

        Top contributors of billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ## Non-Billable Hours ({Q1,Q2,Q3,Q4})

        Top contributors of non-billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ### Changes since previous update

        The sustainability ratio for the previous quarter ({Q1,Q2,Q3,Q4}) was **<percent>%**, so {Q1,Q2,Q3,Q4} brought with it a noticable {increase,decrease} in the ratio of non-billable cell hours.

        <List reasons for changes.>

        ## Changes affecting sustainability over the entire reference period

        Aside from the {Q1,Q2,Q3,Q4}-specific changes described above, there's a few additional factors that affected <cell name>'s sustainability ratio for the entire reference period (i.e., <month> <year> -- <month> <year>):

        <List changes (if any) or delete this section.>

        ## Other notes

        <List notes (if any) or delete this section.>

        ## Projections for (the rest of) <year>

        ...

        ## Plans for improving sustainability

        ...
        ```
      </details>

* Review and update budgets for non-billable cell accounts in [SprintCraft],
  coordinating with epic owners as necessary.

[epic planning spreadsheet]: https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit

#### Cell creation manager

A temporary role to manage the creation of a new cell, where the assignee:

* Creates and self-assigns a dedicated epic.
* Starts a forum thread to coordinate communication on the subject.
* Handles the [step-by-step process of cell creation](../cells/creation_checklist.md) itself.

General information on the life cycle of cells, including how and why they're born, can be found in the [cell life cycle page](../cells/lifecycle.md#cell-birth).

### Code reviewer

As a **code reviewer**:

* I will give prompt, thoughtful, thorough, constructive code reviews.
* (When reviewing OpenCraft work): I will expect that the PR author has done everything outlined in the
  *PR expectations* part of the [pull request process](../processes/pull_request.md)
  section - if not, I will ask them to fix that before I start the review.
* (When reviewing non-OpenCraft work): If I get pinged to review a PR, I will
  respond to it within 24 hours. If it is for a ticket in the current sprint, I
  will ensure that I review it within 24 hours, or at least indicate to the
  author when they can expect a review.
* I will aim to minimize review cycles (especially when reviewing non-OpenCraft
  PRs) by leaving as complete a review as possible. Ideally it should point
  authors to the exact changes needed for their PR to be accepted.
* When reviewing work (or discussing it with the assignee before coding even
  begins), I will make sure that the assignee is not introducing code drift,
  i.e. that everything which could be upstreamed is (planned to be) upstreamed
  as part of the work.
* I will always read through the code diff, considering things like:
    * Is the code easily understandable by humans?
    * Is the code of high quality?
    * Are all relevant coding standards followed?
    * Are all UX / a11y / i18n considerations addressed?
    * Is this introducing tech debt?
    * Is the new code well covered by tests?
* I will always test the code manually, either locally or on a sandbox, unless there is no
  reasonable way to do so.
* I will always check if any updates to the software's documentation are required,
  and either ask the author to update the docs, or ensure the relevant documentation team is pinged.
* If there is any part of the code that I am not confident in reviewing,
  I will indicate that in my comments.
* If there is any part of the code or PR that I particularly like, I will say so - we want to reinforce
  good practice as well as flagging issues.
* I will
  [set up the following template as a "Saved Reply" in GitHub](https://github.com/settings/replies)
  and use it for the "stamp of approval" on all PRs I review:

```markdown
:+1:

- [ ] I tested this: (describe what you tested)
- [ ] I read through the code
- [ ] I checked for accessibility issues
- [ ] Includes documentation
- [ ] I made sure any change in configuration variables is reflected in the corresponding client's `configuration-secure` repository.
```

  Here is a screenshot showing how to conveniently access this template
  when completing a code review using the dropdown button in the top right
  of the review panel (on the "Conversation" tab only):

  ![Review template](../images/review-template.png)

* If I'm the assigned reviewer for someone who is new to OpenCraft,
  I will reserve extra time to provide additional mentoring and support,
  and I will be especially responsive to questions.
  I will also provide feedback to the newcomer which will assist them during their trial period, and raise any major
  issues with their mentor.
* If the assignee is clearly behind the schedule and doesn't respond at all to pings on the ticket or Mattermost within
  48 hours, the code reviewer should determine whether this ticket is urgent. If it is, ask the
  [firefighter](#firefighter) for help to reduce the risk of spillover.

### Newcomer

If I'm **new to the team**, I will:

* Not commit to more than [3 story points](../task_workflows.md#general-tasks) of tasks during my first week, or 5 points
  my second week (so no more than a total of 8 points for the first sprint).  It's really easy to over-commit at first,
  so keep your commitments manageable. You can always take on more work later in the sprint if you finish early.
* Discuss general issues like time management and sprint planning with my mentor.
* I'll prepare for every 121 with my mentor based on the [newcomer 121 checklist](#newcomer-121-checklist)
* Tag my task reviewer(s) with task-specific questions on the ticket, or if I'm completely blocked, try using
  Mattermost to contact my [reviewer](#code-reviewer), [mentor](#mentor), [sprint firefighter](#firefighter), or other people working on the same project.
  Reviewers have time allocated to help during Onboarding, so it's ok to reach out!
* Ask my Reviewer questions **early in the sprint** if there is missing information or if the requirements are unclear.
  [Newcomer-friendly tasks](../task_workflows.md#newcomer-friendly-tasks) have a minimum set of information that should be included in the
  task description, but if this isn't complete, ask the task creator and/or your reviewer to do it.
* Provide lots of updates to my assigned reviewer/mentor for each task,
  so they can provide timely help.
* Assign myself as [reviewer](#code-reviewer) for core team member tasks to learn about the various projects and systems
  that we support.
* Log time spent "onboarding" (reading documentation, learning new systems, setting up devstacks) to my onboarding task.
  Time spent completing the work described in a task can be logged to the task itself, but please take care not to
  exceed the estimated time without **approval from the owner of the parent epic**.  Some leeway will likely exist
  within the epic budget, but it's best to check to be sure.

Also, it is not compulsory to log X hours per week as stated in the contract -- our contracts give an estimated amount
of time expected per week, but the actual work required for each sprint can vary due to many factors.

See the [Process: Onboarding & Evaluation](../processes/recruitment.md) page for more advice on the onboarding, the evaluation
process and criteria.

#### Newcomer 121 checklist

* Ask my reviewers for feedback in the ticket's comments once it's moved to the "Deployed & Delivered" status
* Review feedbacks received on tickets
* Review tickets assigned to me for the next sprint
* Review my onboarding budget
* Ask for feedback about my strengths and the areas I can improve in
* Check if any upcoming tickets lie in the areas I need to improve in, so I can showcase my progress
* Collect issues/blockers to discuss with my mentor

#### Onboarding epic ownership

As a newcomers, I am the owner of my onboarding project, and will:

* Keep time spent by me and my mentor on the onboarding epic to less than 80 hours for the initial 2 month trial period, and less than 130h over the whole trial period in case of extensions.

* The remaining hours of my onboarding budget must be preserved for the core team reviews, and/or onboarding time once accepted to the core team, for a maximum total of 195h.

  Expect around 15 hours to be spent by the core team on the screening and end-of-trial reviews during the initial 2 month
  trial period.  For example, if the screening review requires 2 hours and the end of trial review requires 2 hours for
  each core team member and there are 6 of them, 14 hours (2 + 6*2) is required for those activities.

* Treat my onboarding epic as a proper epic and manage it like an [epic owner](#epic-owner).

  The epic and the budget also includes my onboarding ticket, my mentor's mentoring ticket and any other tickets created
  specifically for my onboarding like the ones for my screening review and the end-of-trial review.

  In the onboarding epic task, there is a `Summary Panel` section on the right side, which shows various details about
  the budget, logged and available time etc. See the below screenshot for an example.

  ![Summary Panel](../images/epic_summary_panel.png)

  Hover over the Time numbers below the progress bar to see an explanation.

* I will create tasks under this epic for distinct pieces of work, set estimates and schedule them with the help of my
  mentor.

  This will help us to track if a particular onboarding/learning task is taking too much time. Since the onboarding
  budget is limited, all the learning time I log on the onboarding epic will be related to the tasks that I work on.

* Log my time with detailed descriptions about the work performed.  For example:

    "Reviewed task and asked questions on ticket, requested repo access, started setting up devstack."

    Or as an example where logging "onboarding" time spent on another task on my onboarding ticket:

    "Onboarding for SE-123: read XBlock Tutorial and configured XBlocks in my devstack"

### Mentor

* I will make sure to inform the team I'll mentor a newcomer before their first day.
* I will familiarize myself with the current [onboarding & evaluation process](../processes/onboarding_and_trial_period_evaluation.md), so I can
  answer questions and help the newcomer through the process.
* I will allocate sufficient time in my sprint, especially at the beginning, to assist and mentor the newcomer.
* I will help the new member in selection of tasks during sprint planning and ensure their assigned tasks meet the goals [specified below](#how-to-select-tasks-for-new-members).
* I will schedule an initial 121 meeting with the newcomer, and a recurring meeting every week.
  These can be spaced out later on if the meetings become less useful. Refer to
  [the mentor checklists page](https://handbook.opencraft.com/en/latest/roles/#mentor-checklists) for the topics to go through.
* For the first 121 with the newcomer, I'll prepare a few interview questions to help in the screening process.
* During every 121 with the newcomer, I'll go through the [mentoring 121 checklist](#mentor-121-checklist) checklist
* I will participate on the screening and developer review tasks, taking into account these
  [evaluation criteria](../processes/onboarding_and_trial_period_evaluation.md#evaluation-criteria).
* I will evaluate the newcomer's usage of the onboarding budget and raise any issues with Xavier. I'll also help the newcomer with
  creating, estimating and scheduling learning tasks in the onboarding epic, tracking them and with
  precise time tracking of his/her tasks if the work logs show too many approximate (i.e. rounded) values.
* I will also explain to the newcomer, the guidelines to manage the onboarding epic and its budget like
  any other epic that we work on and point them to the relevant items in the [newcomer](#newcomer) section.

#### Mentor 121 checklist

* Review the work done by the newcomer since the past mentoring session in prior to the meeting
* Review the onboarding budget status
* Provide feedback about strengths
* Provide feedback about areas of improvement, and suggest methods for improvement
* Review tickets assigned to the newcomer for the upcoming sprint
* Help find tickets for the upcoming sprint if needed
* Discuss current issues/blockers if there are any
* Ask for feedback about the processes and the ease of onboarding; if an issue comes up, open a [GitLab issue](https://gitlab.com/opencraft/documentation/public/-/issues)

#### How to select tasks for new members

Selecting the right tasks for the trail period is very important. The goals are:

* In the first 2 weeks when the new member is trying to understand the different stacks they will need easier tasks to start with.
* From the second sprint they should be picking up at least a couple of moderately complex (5 points) tasks.
* By the end of the trial period there should be sufficient number of (5+) complex tasks to showcase the new member's skills and learning abilities.
* There should be variety in the tasks to allow evaluation for the different types of skills. It is particularly
  important to have 3-4 reasonably complex dev tasks (historically this has been a weak point) and 2+ ops tasks.
* While lines of code is not a measure of complexity having 2-3 thousand lines of code makes it much easier for reviewers to do their evaluation.

* Before the new member's first day they should be assigned a 2 or 3 point task from one of the main projects of the cell. This will allow them to get familiar with the relevant project and be able to pick up more complex tasks in the next sprints.
* The backlog of [Newcomer-friendly tasks](../task_workflows.md#newcomer-friendly-tasks) is a good place to look at first.
* If newcomer-friendly tasks are not available in the backlog, ping the main epic owners of the cell on the onboarding epic asking them if they can prepare suitable tasks. Remind them if needed.
* The evaluation of a new member's skills is incredibly important so feel free to take tasks assigned to any other team member and re-assign them to the new member (unless it looks urgent or has a tight budget).
* If none of the above work, you can create [community contribution tasks](../roles/junior_developer.md#secondary-tasks-community).
* It is okay to assign very small discoveries, which do not result in a standalone epic. (See [Newcomer-friendly tasks discussion](https://forum.opencraft.com/t/newcomer-friendly-tasks/318))
* Tasks which require access to production systems are not recommended for new members. If a task requires deployment, one option is that the reviewer does the deployment, but lets the newcomer know what they’re doing at each step, to help them learn for the future. Any additional access granted needs to be documented in the Onboarding checklist and accesses granted document on their onboarding epic so they may be rotated in case the new member does not make it to the core team.
* During each sprint planning, remember the goals of assigning varied tasks, of increasing complexity.
* If none of the above work, escalate the issue to the [recruitment manager](#recruitment-manager).

* If the new member does not make it to the core and it is their last sprint it is definitely better to assign them tasks that they might be able to complete without wasting budget hours, even if it's from the onboarding budget, but if this type of task isn't available, we can then focus on giving them tasks that aren't urgent, ie that can be rescheduled for the following sprint when they leave, to limit the impact on the rest of the team.

#### Mentor checklists

##### Week before

The week before the newcomer first day, the mentor is tasked with finding or creating a meaningful task for the first sprint.

We must ensure the newcomer has a task with enough complexity in his first sprint for a screening review. While this task won't be enough to evaluate the all the different abilities required such as frontend, backend, devops, epic management etc, it should be complex enough to show the newcomer can work independently and deal with technologies we use on our day-to-day.

The newcomer should create a PR as soon as possible and push to it daily, at least, so we can better help when stuck and review the work progress.

One example of such task is one fixing a bug in the master branch of edx-platform. It involves reading and understanding code, provisioning the master devstack, running tests, creating a PR and discussing with the reviewer.

We might not have such a task in our Backlog, so make sure to ping epic owners if you can't find one. The [INCR](https://openedx.atlassian.net/projects/INCR/issues) and [CRI](https://openedx.atlassian.net/projects/CRI/issues) upstream projects are excellent sources of tasks as well.

##### Initial 121

This meeting objective is to get the mentor to know better about the newcomer capabilities and help in the screening process. Here are some example questions to help with that:

* Are you comfortable working in a POSIX environment?
* Do you have experience coding in Python? How many years?
* What is your skill level in frontend development? Do you have experience with any frameworks?
* How do you feel working 100% remotely? Is this your first experience?

##### Weekly 121s

These meetings will be used mostly for answer questions, both technical or related to OpenCraft processes.

* Evaluate the newcomer's current sprint and tasks currently at risk of spilling over.
At-risk tasks should be flagged with the sprint firefighter, to avoid missing delivery deadlines.
* Help the newcomer prioritize work and check if there are any difficulties working remotely.
* Provide feedback, both on technical and other skills such as communication, time management etc. This feedback may come from other team members, or from my observations on the newcomer's assigned tasks.
* Help the newcomer use accurate task statuses during their sprints to ensure the Sprint Commitments spreadsheet for
their cell is accurate when it comes time to do the developer review. If there are any issues with what is recorded
there, let the [Sprint manager for your cell](../cells/list.md) know.

### Firefighter

By default, all cells have two firefighters for each sprint, designated as Firefighter 1 and 2.
Project cells are allowed to define [their own firefighting regimen](#project-cell-firefighting),
but if no [custom rules](../cells/cell_specific_rules.md) are defined, the following continue to apply.

The two firefighter roles have exactly the same responsibilities.  As a **sprint firefighter**:

* I will keep at least 15 hours of time for sprint firefighter duties as
  described below, and will proactively pursue those duties.
* I will assign tasks for the rest of my sprint hours normally, but I will also ensure a few additional tasks
  are left either in the "stretch goals" or the following sprint during sprint planning, in case there
  isn't enough firefighting work to fill my hours. I will only pull these tasks into the current sprint
  if I am confident that I will have time to finish them in addition to the firefighting. These tasks
  should be assigned to me and have a reviewer, to be ready to pull in.
* If I am the firefighter for the first week, I will arrange a Social Chat by posting a topic and a time (the time should be the same first week of the sprint) on the [forum](https://forum.opencraft.com/t/social-chats-meetings).
* I will not record any time on the Sprint Firefighter
  task directly (always use a dedicated client story/bug/epic ticket).
* I will be subscribed to the
  [help](https://mail.opencraft.com/postorius/lists/help.opencraft.com/)
  and
  [urgent](https://mail.opencraft.com/postorius/lists/urgent.opencraft.com/)
  mailing lists (be sure to filter them to a separate folder
  to look at them only when you need to - but also make sure
  that if any such emails also explicitly include you in the To/CC,
  then they will arrive in your inbox).
* I will [add myself to the pager rotation](https://courses.opencraft.com/courses/course-v1:OpenCraft+onboarding+course/jump_to/block-v1:OpenCraft+onboarding+course+type@vertical+block@first_steps_as_a_core_team_member_firefighting_pager_setup), at least during my work hours the weeks
  I am firefighter, plus any additional time I optionally want to help cover.
* I will work on the following, listed by decreasing priority:
    * Handle emergencies from other team members
      (reported by team members directly to me or to the other sprint
      firefighter, with Braden arbitrating priorities)
    * Handle emergencies reported by clients from the cell--these will come in through the
      help@opencraft.com mailing list, which everyone receives.
      Firefighters triage and prioritize these issues, asking other cell members for advice
      as needed, and escalating to Braden/Xavier if everything else fails.
    * Handle critical bugs reported by QA
    * Deploying hotfixes/security fixes on client instances
    * Follow up on issues affecting [periodic build](https://manage.opencraft.com/admin/instance/openedxinstance/?q=periodic+build) instances
      as necessary (when prompted by the team member responsible for [Community Liaison](#community-liaison)).
    * Provide reviews for tasks that are missing a reviewer.
    * Complete any personal spillover from the previous sprint
    * Work on client requests that can't wait until the next sprint, in particular in
      the first week of a sprint.
    * Help ensuring a clean sprint by helping other team members, in particular in the
      second week of a sprint.
    * Being available on the Open edX Slack (#general), answering questions and
      responding to emergencies reported there (log time on OC-1017)
    * Watch over sandboxes (ie keep an eye on the instance manager
      on a regular basis, few times a day, to ensure they build correctly, and debugging if needed)
    * [Document incidents as they happen](https://forum.opencraft.com/t/learning-devops-refresher-tasks/472/23) and post
      a summary to the [OpenCraft Ops Review forum thread](https://forum.opencraft.com/t/opencraft-ops-review/533).

#### Project cell firefighting

As [with vacations](../processes/vacation.md#project-cells), a project cell is allowed to define its own firefighting regimen.
If no [custom rules](../cells/cell_specific_rules.md) are defined, though, the default applies.

Custom rules are always subject to the following constraints:

* The rules must be reviewed and approved unanimously by all of its members and the CEO during the cell's inception, and at any time thereafter when a change is proposed, as a change request to this handbook.
* The default OpenCraft client-facing SLA of 24 hours always applies.  It can only be replaced by a stricter SLA.  Rotations should be built with that in mind.

Note that regardless of custom rules, in cases where firefighting help from a generalist cell becomes necessary, time should be logged in such a way as to use the project cell's budget.

#### Dependency upgrades for security issues

  GitHub sends the security alerts by scanning (by default, all public repositories are scanned; for private repositories, the admins have to explicitly permit the scanning) the repository dependencies, and based on the severity levels:

  * `Critical and High level` severity vulnerabilities: As there could be a risk of compromise or significant downtime for users, these vulnerabilities must be patched as soon as possible. The FFs should create the tasks for applying the patch and start working on them right away.
  * `Medium level` severity vulnerabilities: Report them and create tasks for patching them so that team is aware of them. These tasks can be scheduled for the next sprint and if any of the FFs have time, they can work ahead on these tasks.
  * `Low level` severity vulnerabilities: These vulnerabilities should be reported and tasks for patching them created with a lower priority. These tasks can be scheduled for a future sprint and prioritized by the epic owner as appropriate.
  * `Undefined or unclear` severity: Vulnerabilities or security fixes whose severity is unknown should be reported and discussed with team before we can take any action.
  * `Dependabot PRs`: Dependabot PRs for bumping the version of vulnerable dependencies or security fixes should be handled by the FFs based on the process mentioned above for various severity levels.

  The severity level mentioned in a vulnerability report may not always match our own assessment (`critical issues` in a disabled feature may not be `critical`). Hence the processes around categorization of the security vulnerabilities and the assessment of their severity levels need to be expanded and improved over time.

* Work on additional tasks from the backlog if additional time is available

* Well before the end of the current sprint, at the start of a new sprint (every
  second Monday), I will remind the epic owners to get every ticket in the
  upcoming sprint "green" (has an assignee, reviewer, and points) by pinging
  people as needed, and I will help make sure all tickets are green before the
  start of the sprint.  If my timezone makes this challenging, I will coordinate
  with Firefighter 2 to finish up on Monday.  The time spent getting a ticket to
  green will be logged on the ticket.

* On Friday or Monday well before the mid-sprint update, I will do a sprint
  checkin on all "backlog" and "In progress" tickets (this means to post a
  comment on each ticket to ask how the assignee is doing and if they need help,
  and/or asking them to update the status of the ticket if it is not clear).

### Community Relations

One of OpenCraft's most important core values is community involvement.  To reflect this, we have two roles that are focused on community involvement:

#### Core Contributor

edX has a [Core Contributor Program](https://openedx.atlassian.net/wiki/spaces/COMM/pages/3143205354/Open+edX+Core+Contributor+Program) which allows people outside of edX to review and merge OSPRs, contribute to platform development in other ways (e.g. reviewing translations), mentor new community members, and participate in the strategy and direction of the platform.

Our goal is for every OpenCraft developer to eventually become a Core Contributor and for every OpenCraft Core Contributor to spend a minimum of 20 hours per month on contributions as a Core Contributor.

We set priorities that we want our team's Core Contributors to focus on - see the [Core Contributors Epic description ("Priorties for Core Contributor Work")](https://tasks.opencraft.com/browse/MNG-2649) for the current list of priorities. Besides the priorities, it's important for Core Contributors to spend time every month on:
* Community: Answering community questions (on the Open edX [forum](https://discuss.openedx.org/) and [Slack](http://openedx.slack.com/)), mentoring other developers, etc.
  * Generally favor helping one person more fully, over having many smaller contributions to more threads.
* Maintenance: Updating documentation, fixing bugs, updating packages, improving developer onboarding, etc.

In addition, **all team members** are encouraged to be involved in the Open edX community on the forum and helping with platform maintenance as described above, even if you're not an official Core Contributor. If you spend some time on community involvement and it doesn't fall under the scope of one of your current tickets or roles, just create a ticket for that in your sprint (e.g. "Answer XBlock question on forum") and use the "Contributions" account.

#### Community Liaison

This role involves the heaviest commitment of the Community Relations roles, at **8 hours a week**, and is meant to be
taken by a single person across all cells.

Work on the role is to be assigned by default to the "Community Relations" non-cell account, so as to alleviate the
budget burden on the Community Liaison's cell.  However, the following applies:

* Whenever possible the Liaison should assign individual tasks to other accounts, using the "Community Relations" only as a backup.
* The "Community Relations" account can only be used on tasks assigned to the Community Liaison themselves.  In other
  words, it should **not** be used for core contributor tasks, or, for example, for other cell members to attend community meetings.


##### OpenCraft Open Source Community

* Monitor OpenCraft's public repos
  * Specifically, these areas on [GitHub](https://github.com/open-craft?q=&type=public&language=) and [GitLab](https://gitlab.com/explore?sort=latest_activity_desc&visibility_level=20):
    * [List of open issues](https://github.com/search?q=org:open-craft+is:public+is:issue+state:open) (GitHub)
    * [List of open PRs](https://github.com/search?q=org:open-craft+is:public+is:pr+state:open) (GitHub)
    * [List of open issues](https://gitlab.com/groups/opencraft/-/issues) (GitLab)
    * [List of open PRs](https://gitlab.com/groups/opencraft/-/merge_requests) (GitLab)
  * Monitor these channels for technical questions (e.g. about our XBlocks) and answer them. Watch for community pull
    requests and either review them or assign them to a developer for a future review (let the contributor know that
    the review has been scheduled).
  * Ensure that bug reports, PRs and other communication directly submitted to us by the community get a prompt
    answer and review.  "Prompt" is defined as:
    * **24 hours** for direct acknowledgement by the Liaison.  The Liaison should set up a communication filter so that they
      are immediately notified of incoming communication from the community (as opposed to OpenCraft's own chatter in
      PRs and issues.)
    * In case of a bug report or pull request, the Liaison will also create an internal task upon acknowledgement and
      set a deadline of **2 weeks** from creation for the team to review and, if possible, fix the bug or merge the PR.
      A firefighter will be assigned to the task upon creation so that work on it can start immediately.  (Yes, this
      should be treated as a fire: contributions are rare enough that it's worth acting quickly to encourage them.) If
      the task is complex or requires domain knowledge, the firefighter will do a first superficial pass immediately to
      show the contributor that we care, and then mention that another pass by someone else will need to happen, and
      when. Even in these cases, though, an early sprint deadline will be set for the review, and the assignee will be
      asked to remain reactive to subsequent updates and comments from the contributor.


##### General community relations

The Community Liaison will both engage in and help coordinate community-facing efforts in the company as needed, across
all contact points.  Specific duties include:

* Help coordinate OSPR reviews:
  * Maintain the prioritized list in a document shared with the OSCM team:
    [OSPR priorities from OpenCraft](https://docs.google.com/spreadsheets/d/1DPfQg8O09PqW4BnQTJdE3FwvWb_EzbGiE74cG3LkP7w/edit)
  * Send the top 3 on the list to the edX OSCM team (usually Natalia) once a week.
  * Log time on [SE-1631](https://tasks.opencraft.com/browse/SE-1631).
  * Keep 0.5hrs/week available to handle any PR sandbox requests by edX.
    * Someone (likely [Ned](https://github.com/nedbat) or [Natalia](https://github.com/natabene)) will ping you on a PR targeting an older release branch (currently limited to Hawthorn and later).
    * Make sure the PR has a reviewer assigned. If not, request that one be assigned before continuing with the next step.
    * Create a new sandbox for the PR, and link the sandbox URLs to the PR.
      * To do this you can use the ``setup_pr_sandbox`` Ocim management command and provide it the URL of the edx-platform PR.
    * In case there isn't sufficient time, create a ticket and ping the [firefighter](#firefighter) to do the actual sandbox creation steps.
    * You can give SSH access to the reviewer by adding their GitHub ID to ``COMMON_USER_INFO`` in the PR sandbox's config:
      ```yaml
      COMMON_USER_INFO:
        - github: true
          name: github_id
          type: admin
      ```
    * You can ask the PR author to add the standard setting section to the PR description with the above config. Otherwise you may need to add this back each time the sandbox is updated from the PR.
* Monitor the Open edX [Slack](http://openedx.slack.com/), in particular the [#opencraft channel](https://openedx.slack.com/archives/C0F63UDL0)
* Help to moderate the [official Open edX forum](https://discuss.openedx.org/)
  * Welcome new posters
  * Help people use the forums well, including giving advice where applicable to:
      * Choose the right categories, and move/split/concatenate threads when needed.
      * Provide enough information about their situation - we can help them to refine their questions, asking them
        questions to make their requests more precise
      * Use existing resources or documentation
  * Review posts flagged automatically by Discourse (for example, someone finishing a post too quickly after creating
    their account)
  * Make sure "Open edX" is spelled (and capitalized) correctly in post titles. Don't bother with the body of posts, but
    the topic lists should show only show the correct usage.
  * Set the right tone for discussions and, in general, be a model Open edX citizen
* Monitor OEPs, ADRs, and community discussions on forum, and ensure that OpenCraft participates in relevant ones
* Keep an eye on the [build-test-release CI notifications](https://groups.google.com/g/open-edx-btr-notifications).
  Forward issues to firefighters as necessary, and ensure we post replies on failures, either explaining them when
  we fixed or caused them, or otherwise asking for help to fix them.
* Be available as a community reference ("Talk to Ned about this, Felipe about that") not only
  internally at OpenCraft, but also for the community itself.

##### Attending community meetups

Involvement is expected in the following community meetups as noted:

* [Biweekly Contributor's Meetup](https://discuss.openedx.org/t/open-edx-contributors-meetup/1450): organize the
    meeting, including scheduling it, announcing it in the forum, preparing the agenda, holding the meeting, and finally
    posting the recorded video.
* [Monthly Community Hangout](https://openedx.atlassian.net/wiki/spaces/COMM/pages/590184858/Online+Community+Meetups):
    in addition to attending the meeting itself, organize OpenCraft's participation, including suggesting topics and
    inviting members of the team to speak when the opportunity arises.

##### Community project management

The Community Liaison is expected to follow-up on and otherwise refine tasks (i.e., do project management for the
community) in the following boards.  This is not limited to tasks where OpenCraft is directly involved, but should also
include tasks created by or assigned to other community members:

* [Community working group board](https://trello.com/b/d8oWzNoy/community-working-group)
* [Core Contributor program board](https://github.com/orgs/edx/projects/1)

##### The Core Contributor Program

The Community Liaison also takes ownership of the [core contributor program epic](https://tasks.opencraft.com/browse/MNG-2649).
If the Liaison is not yet a core contributor, they must work to become one, so that they can credibly help manage the program.

### Ops reviewer

1. Receive ops@opencraft.com and the pager alerts - check alerts/emails to ensure the alerts
   have been properly handled by the other recipients and nothing has slipped through.
1. Be a backup on the pager (alerts sent first to the other(s) recipient(s), but escalated if not
   acknowledged within 30 minutes).
1. If none of the other recipients are around and an issue is left unsolved, and it either is sent
   to urgent@, or was sent more than 12h ago to ops@opencraft.com and needs to handled
   quickly, warn the [sprint firefighter](#firefighter) about it: create a ticket about the issue and assign it.

Note that keeping an eye on [build-test-release CI notifications](https://groups.google.com/g/open-edx-btr-notifications)
triggered by periodic builds and following up as necessary is part of the [Community Liaison](#community-liaison) role,
and out of scope here.

### Discovery duty assignee

This role is specific to a generalist cell: because of the focus on a single client, project cells need not have a developer on standby for new prospects.

As the **week's discovery duty assignee**:

* I will make sure I am the right person to take on a specific discovery, if I feel I am not
  I will exchange work with other people from the sprint, to get someone else
  to do it, while I help them with their tasks. It is very important to be able to estimate a
  task well, we should make a conscious effort to assign the task to the person who can best
  handle it, if possible (if the person is available and willing to do the work).
* I will keep at least 5 hours of my time to do discovery work on any small leads/client
  requests that come up during the week. I may still plan some tasks to pull into the sprint
  if no fires pop up. Before pulling tasks in the sprint, I will use these hours to help others finishing
  their sprint commitments.
* If I do some discovery tasks, I will ping the [sprint firefighters](#firefighter) to review the result.
* As always, if I do the discovery, I understand that I would be expected to complete the epic
  work later on if the client accepts it.
  (To avoid people being forced to commit to being the owner of big epics, any
  huge discovery stories should always be scheduled into future sprints rather
  than directly assigned to the discovery duty assignee.)
* I will ensure an additional task is left either in the "stretch goals" or the following sprint
  during sprint planning, in case there isn't enough discovery duty work to fill my hours. I will
  only pull this task into the current sprint if I am confident that I will have time to finish it
  in addition to the discovery duty. This task should be assigned to me and have a reviewer, to
  be ready to pull in.

### Specialist roles

Specialists are people in the cell with more context and understanding about a certain topic, such as DevOps, Ocim, or Analytics/Insights.
The goal of having this role is to allow cells to self-sustain and run its own projects without being blocked on context from people from another cell.

While specialists have priority on taking tasks related to its specialty, there's no restriction on other team members and taking tasks is encouraged to spread knowledge around the cell.
Specialists should always be at least the 2nd reviewer so they are in a position to track the ticket and provide help.

Specialists are not only responsible for handling tasks within the cell, but also for coordinating with other specialists to discuss and schedule improvements when necessary.

The next sections cover specific tasks to be done by each type of specialist:

#### DevOps Specialist

The specialist should always be involved in some way in the devops tasks of the cell, if not as the assignee, then as a reviewer or 2nd reviewer.

 They also have priority to take DevOps tasks as assignee as much as they want, they don't have to take everything (neither should they try), but they should be able to use the context of tasks, especially client tasks, to implement useful work for the infrastructure as a whole.

 Client owners are still responsible for handling DevOps tasks of their client's instance. The specialist should help, advise and review the tasks, while the epic/client owner creates the tasks and handles getting them assigned.

Responsibilities within its cell:

1. Assign, take or review DevOps related tasks.
1. Support team members on DevOps related tasks, provide information about deployment mechanisms, sidecar services, and any particular detail that might affect the outcome of the task.

Responsibilities shared between DevOps specialists across cells:

1. Create tickets to maintain and update common server infrastructure and its related deployment mechanisms (MySQL, MongoDB, Consul, Vault, Prometheus, etc).
1. Create discovery tickets and epics to improve infrastructure, taking into account the current epic schedule and throughput available for non-billable DevOps work.

#### Technical Security Manager

* Responsible for the **technical security** of the work we do, our platform and OpenCraft as
  a whole.
* Responsible for **[OpenCraft's Security policy](http://opencraft.com/doc/handbook/security_policy/)**.
* **Offboarding** of team members who have left.

### Epic owner

As an **epic owner**:

* I will make sure the epic has tasks with time estimates and a global budget. A discovery will likely be required for this - read the [estimates](../how_to_do_estimates.md) section and follow the steps listed there.
* I will ensure the "Timeline" section of the description and the epic due date
  are kept up to date. The "Timeline" section should include a list of
  milestones and deadlines for each (these milestones could be as simple as
  "check in with client"). The due date should be set to the closest
  deadline, and must be revised once each deadline is past/updated.
* I will keep clients updated on the status of tickets
  (for clients with long term monthly contracts, this means updating
  their JIRA or Trello boards as work progresses).
* I will attend meetings with the client about the project as required,
  e.g. weekly scrum meetings.
  (We try to limit these sort of meetings to once a week per client at the most.)
* I will create tickets on OpenCraft's JIRA board for upcoming work,
  and place them into the appropriate upcoming sprints
  so that we can get the work done well ahead of applicable deadlines.
    * If a ticket without point estimates is scheduled for the upcoming sprint,
      and an estimation session is open, I will make sure to add it to that session,
      so everyone on the team can help estimate it.
* I will aim to split out any suitable newcomer-friendly work into separate tasks, flag them as
  [Newcomer-friendly tasks](../task_workflows.md#newcomer-friendly-tasks) in the Backlog, and ensure they contain the required information.
  To guard the epic budget, "onboarding" time can be logged against the newcomer's epic.
* If applicable, I will create corresponding tickets on clients' JIRA or Trello boards
  and link to those tickets from the description of internal tickets as necessary.
  (In many cases, a single internal ticket from OpenCraft's JIRA
  will correspond to a single ticket from a client's JIRA or Trello board.
  But there are also cases where a single internal ticket might map to
  multiple external tickets or vice versa.)
* I will set the "Original Estimate" of each ticket to the number of hours
  listed in the discovery document. If a ticket covers a larger story
  from the discovery document only partially, I will set its "Original Estimate"
  to an appropriate fraction of the number of hours listed in the discovery document.
  For any ticket that does not directly correspond to a story from the discovery document,
  I will set the "Original Estimate" based on the amount of time that I think
  will be required to complete the work, taking into account the number of hours
  that remain in the epic's budget at that point in time.
* On Thursday in the second week of a sprint, I will post an epic update
  that answers these questions:
    * For epics with a fixed scope/budget:
    ```text
    h2. Epic Update -- fixed scope / fixed budget (Sprint ???)

    h3. Budgeting

    h4. What's the status of the hour budget for this epic?

    We've used __ hours of the __ hour budget.

    h4. Will it be possible to complete the remaining work within the hour budget?

    {color:#c1c7d0}
    * Make sure to set the _Original Estimate_ on each ticket based on the corresponding estimate(s) from the discovery. This will help you detect potential savings or -- more importantly -- potential cases of budget excess if the assignee of a ticket specifies a different estimate via the _Remaining Estimate_ field later on.
    {color}

    (/) or (x)

    *If not:*

    h5. {color:#de350b}How many additional hours will be required to complete the remaining work?{color}

    (specify)

    h5. {color:#de350b}Has the client agreed to pay for any hours exceeding the current budget (and if so, how many)?{color}

    {color:#c1c7d0}
    * For internal epics using non-cell accounts, Xavier is the client. Specify the number of additional hours that Xavier approved (if any).
    * For internal epics using cell accounts, coordinate with the cell's sustainability manager.
    {color}

    (/), (specify) or (x)

    h3. Capacity planning

    h4. Will it be possible to complete the remaining work before the deadline?

    {color:#c1c7d0}
    * Create tickets for items listed in the discovery and spread them out across upcoming sprints. (Or, if the tickets already exist, simply adjust the scheduling for upcoming sprints as necessary). Use the epic description for this if you need to schedule tickets for future sprints that don't exist in JIRA yet. This will help you determine whether the epic is on track progress-wise.
    {color}

    (/) or (x)

    *If not:*

    h5. {color:#de350b}Has the client approved a new deadline for completing the remaining work (and if so, what is the new deadline)?{color}

    {color:#c1c7d0}
    * For internal epics using non-cell accounts, Xavier is the client. Specify the new deadline that Xavier approved (if applicable).
    {color}

    (/), (specify) or (x)

    h5. {color:#de350b}How many hours will we need to allocate to this epic in the coming months to complete the remaining work?{color}

    (specify)

    h3. Epic management

    h4. Are the deadlines in the "Timeline" section of the epic description correct?

    (/) or (x)

    h4. Are relevant details mentioned in the remaining sections of the epic description up-to-date?

    (/) or (x)

    h4. Is the status of each story in the epic correct?

    (/) or (x)

    h3. Sprint planning

    h4. Are all upcoming stories that we should work on in the next week or two created and in the correct upcoming sprint?

    (/) or (x)

    h3. Other Notes

    ...
    ```
    * For epics with recurring monthly budgets:
    ```text
    h2. Epic Update -- dynamic scope / monthly budget (Sprint ???)

    h3. Budgeting

    h4. What's the status of the monthly hour budget for this epic?

    We've used __ hours of the monthly __ hour budget.

    h4. Will it be possible to complete the remaining work for this month within the monthly hour budget?

    (/) or (x)

    *If not:*

    h5. {color:#de350b}How many additional hours will be required to complete the remaining work?{color}

    (specify)

    h5. {color:#de350b}How are we going to deal with this?{color}

    +Scope reduction+: We will *adjust the volume of work* for the coming month(s) to make up for this month's increased budget usage.

    or

    +One-time budget extension+: We will ask the client to pay for hours logged in excess of the budget cap *this month*.

    or

    +Permanent budget extension+: The volume of work requested by the client is generally increasing, so we will ask for a *permanent increase* of the monthly budget.

    h5. {color:#de350b}Has the client agreed to the selected measures (scope reduction and/or budget extension)?{color}

    {color:#c1c7d0}
    * For internal epics using cell accounts, coordinate with the cell's sustainability manager.
    {color}

    (/) or (x)

    h3. Capacity planning

    h4. Will we need to significantly increase or decrease the hour allocations for this epic in the coming months to handle the volume of work?

    (/) or (x)

    *If so:*

    h5. {color:#de350b} How many hours should we allocate to this epic from next month on?{color}

    (specify)

    h3. Epic management

    h4. Are the deadlines in the "Timeline" section of the epic description correct (if any)?

    (/) or (x) or N/A

    h4. Are relevant details mentioned in the remaining sections of the epic description up-to-date?

    (/) or (x)

    h4. Is the status of each story in the epic correct?

    (/) or (x)

    h3. Sprint planning

    h4. What's going on this sprint?

    (describe)

    h4. What's going to be worked on next?

    (describe)

    h4. Are all upcoming stories that we should work on in the next week or two created and in the correct upcoming sprint?

    (/) or (x)

    h3. Other Notes

    ...
    ```
    * For ad-hoc support epics:
    ```text
    h2. Epic Update -- dynamic scope / no specific budget (Sprint ???)

    h3. Budgeting

    h4. Are all hours that we logged against this epic's account last month billable?

    {color:#c1c7d0}
    * Hours may be non-billable if they exceed agreed-upon budgets for specific tasks: If the client approved a specific budget for a task or set of tasks, we need to stop and ask for a budget extension before spending more time on the task(s). If we ask for retroactive approval, there is a chance that the client won't agree to pay for any/all hours exceeding the original budget, which will make these hours non-billable.
    {color}

    (/) or (x)

    *If not:*

    h5. {color:#de350b}How many non-billable hours did we log last month?{color}

    (specify)

    h3. Capacity planning

    h4. Will we need to significantly increase or decrease the hour allocations for this epic in the coming months to handle the volume of support requests?

    (/) or (x)

    *If so:*

    h5. {color:#de350b} How many hours should we allocate to this epic from next month on?{color}

    (specify)

    h3. Epic management

    h4. Are relevant details mentioned in the epic description up-to-date?

    (/) or (x)

    h4. Is the status of each story in the epic correct?

    (/) or (x)

    h3. Sprint planning

    h4. What's going on this sprint?

    (describe)

    h4. What's going to be worked on next?

    (describe)

    h4. Are all upcoming stories that we should work on in the next week or two created and in the correct upcoming sprint?

    (/) or (x)

    h3. Other Notes

    ...
    ```
  *Other Notes* is for any concerns, questions, upcoming vacations, or anything else
  that you think is worth mentioning because it could affect planning
  of the next sprint or two.
* On Thursday or Friday in the final week of each sprint, I will assign
  upcoming stories from this epic that need to be done next sprint
  to myself - or I will ask others from my cell to be either the assignee or the reviewer.
* If I'm going on leave, I will follow the [vacation procedure](../processes/vacation.md), and hand off my responsibilities for my epic to
  Epic Reviewer.

### Epic Reviewer

Epic reviewers are assigned to the 'Reviewer' field on an Epic ticket. As an **epic reviewer**:

* I will keep up to date on the progress of the epic.
* I will review discoveries, planning, budget, and documentation written at the top level of the epic.
* I will read each epic update and give feedback as needed.
* I will be ready to perform the tasks and duties of an epic owner in case they go on vacation or are otherwise
  unavailable for an extended period.

### Client owner

* Client owners manage their relationship with their clients. All the epics and tasks from a
  given client are done within a single cell, the one the client owner belongs to, but see
  [cross-cell collaboration](../organization.md#cross-cell-collaboration) for some nuances around
  task reviews.
* Initial contact with prospects, estimations work. Note that a portion of Business development's
  time is assigned to each cell to do most of the initial contact and quote work with prospects.
* Client owners keep the [Client Briefings](https://handbook.opencraft.com/en/latest/working_with_clients/#client-briefings) up to date.

For each client, we have a single person who is designated as the owner for that client, called
the "client owner." The client owner is responsible for handling most communications with that
client. The client owner is designated in [OpenCraft's CRM](https://opencraft.monday.com/boards/1077823105/),
along with the contact details and background for the client.

In general, regardless of which email address the client sent their question to, the client owner
should be the one who replies, though they may hand off any conversation to anyone on the team as
needed (such as their [cell's firefighter](#firefighter)). If someone else received the email, please "Reply All" to pass the
message on to the client owner for them to reply (add the client owner to the "To" field, and add
"OpenCraft <help@opencraft.com>" to the CC field, and say something like "Passing this on
to (client owner name)").

There are a few exceptions:

* If it's an urgent problem, whoever sees it first should reply and CC the [firefighters](#firefighter),
  help@opencraft.com, and the client owner. One of the firefighters should respond.
* If it's a general issue/question:
    * If the email is related to a specific project/epic with a different epic owner, that epic
      owner can reply directly (make sure the client owner and help@opencraft.com are CC'd).
    * If the client owner is away, their designated backup person (usually the reviewer on the
      "Support" epic/ticket for that client) should take over this role. (Or the firefighters
      if no backup person was planned.)

We have a few clients with monthly development budgets. For those clients, the assigned client owner
is also responsible for reviewing the monthly budget before and during each sprint,
to ensure that we are not going too far above or below the budget. They should refer to the
[SprintCraft] which can monitor and project the status of each budget as
of the end of the upcoming sprint.

When we get a new client, or when the client owner changes (or we start working with a new
person coordinating things on the client's side), we send them
[a welcome email](https://docs.google.com/document/d/1TFBgmq5QA318IfVLBbg9owuqyVl7e4UX5ZWsFQl-u6o/edit)
explaining who the client owner is and how to contact us.

Client owners are in a prime position to anticipate the needs of our clients, which can help us start new projects with them. Read the (private) [Sales Opportunity: developer stories] forum thread for examples of how we've created solid client
relationships and handled challenging situations. Take a bit of time each sprint thinking about new ways we might create value for our clients.

[Sales Opportunity: developer stories]: https://forum.opencraft.com/t/sales-opportunity-developer-stories/1055

### Cell Supporter

**Role type**: This is a full time non-cell role.

**Description**: The main purpose of the Cell Supporter role is to help lighten the load of management work
that cell members have to take care of, so that they can focus more on coding/DevOps, and on handling their clients/epics.
The purpose of this role is not to take away cell members’ autonomy in making decisions about their projects,
nor to introduce another layer of management between them and their clients, nor to centralize cell-level decision making
in a single role. As mentioned previously, we believe that the people who are closest to a given client or project
are in the best position to make decisions about how to handle and progress that project.

*Note that cell supporters are not project managers, and they also do not manage client relationships.*

These duties are part of [epic ownership](#epic-owner) and [client ownership](#client-owner),
which are responsibilities to be taken on by core team members of individual cells.

#### Responsibilities

Responsibilities of a **cell supporter** include:

##### Cell management support

* Helping cells by taking on [cell management roles](#cell-member) that they are currently unable to fill themselves
(e.g. due to lack of interest or capacity).
    * Cell members who express interest in taking these roles get precedence over cell supporters
      when it comes to assigning cell management roles.
    * Cell supporters should not take all cell management roles for a specific cell,
      even if it would be possible to do so without impacting a cell supporter's capacity
      for fulfilling the remaining responsibilities listed below.
    * As a cell’s capacity and/or interests change, cell supporters may hand cell management roles back to the cell.
      To make sure that cell management roles do not remain assigned to cell supporters indefinitely,
      they perform a quarterly check-in to see if cells wish to take back any of their delegated roles,
      and if there are other roles that the cells need to delegate instead.
* Coordinating training initiatives for cell management roles, and making sure documentation on cell management roles
  is kept up-to-date.
    * For example, prior to a pending cell split, cell members that will be taking on a cell management role
      for the first time should get a chance to familiarize themselves with the new role
      before having to jump in head-first and being expected to handle the role according to our standards.
* Helping cell managers address specific challenges requiring cross-cell coordination.
    * For example, if more than one cell is facing capacity issues, it may be necessary to shuffle
      projects around between cells. The effort involved in leading this type of cross-cell coordination
      goes beyond the amount of work that is usually involved in epic planning (and when capacity is low
      it may be especially difficult to find time for additional planning work), so cells may decide
      to delegate this work to a cell supporter.

*Notes on capacity and budgeting:*

* As indicated above, the amount of cell management support that a cell supporter commits to
  should leave room for fulfilling the remaining responsibilities listed below.
* Cell management epics and tickets will stay with their respective cells, even if they are
  temporarily delegated to a cell supporter. This means that in terms of sustainability impact
  there is no difference between having cell members handle cell management roles and
  delegating them to cell supporters.

##### Process improvements

* Conducting process reviews, ideally at regular intervals, with the goal of keeping "process debt" in check.
* Coordinating work on implementing process improvements based on results of process reviews.
* Addressing individual shortcomings of internal processes that surface over time (such as sustainability
  blocking development, how to deal with sustainability in the context of cell splits, and
  improving coordination between cells and business development/marketing).

##### Cross-cell planning

* Taking care of scheduling upgrade epics:
    * Keeping track of the rotation of responsibility for upgrades between cells.
    * Informing epic planning managers of cells ahead of time that their cell’s turn is coming up,
      and that they’ll need to work on freeing up capacity as necessary.
* Helping with cross-cell coordination on scheduling epics from [Ocim & DevOps Epics Schedule (v2)](https://docs.google.com/document/d/1Fy-3au6-WbjpM4a_i_RtCV6ba_XdA29dcjctM9bSEkA/edit).

##### Other responsibilities

* Helping the Open edX community in one of the [non-technical core contributor roles](https://openedx.atlassian.net/l/c/QEEtuNeo).
* Acting as a backup for other non-cell roles (based on interest/availability).

### Business Development Specialist

As a **business development specialist**:

* I will ensure clear and prompt communication with prospects, leads and clients.
* I will answer emails sent to  contact@opencraft.com.
* I will answer leads sent to our CRM web-to-lead form.
* I will regularly look out for business opportunities. Strategy and time spent on these tasks should be discussed with the cell sponsoring the
  prospect work, and possibly Xavier for key prospects.
* When a lead becomes a prospect, I will follow-up by providing information,
  drafting quotes, creating epics and issues and documenting in Jira so the team can follow
  what’s happening. I will include the template in the epic description ("Context, Scope,
  Budget, Timeline") and set the time budget on the epic ticket.
* I will handle the onboarding of confirmed clients: tell them how to work with us, such as when/how
  to use the urgent@opencraft.com email address, when/how to use help@opencraft.com, who
  their main contact(s) should be, how billing works, etc. I will write the initial
  [Client Briefing](https://handbook.opencraft.com/en/latest/working_with_clients/#client-briefings) in our CRM.
* When working on a new project or client, I will handle initial communication and coordination
  between the client and the cell working on the project (until the kickoff/handover to the client owner).
* Generally, when writing to the clients, I will always make sure there is either contact@, help@, or billing@ CC'd:
    * anything related to leads, prospects, and quote should be shared with contact@opencraft.com
    * anything related to invoices and billing should be shared with billin@opencraft.com
    * the rest (anything non-financial) should be shared with help@opencraft.com
* I will check completed discoveries for any estimates that can be reused for future discoveries and quotes,
  add them to the [Price list](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=1171479307)
  sheet of the epic planning spreadsheet, and add/update price items in Proposify as necessary.

### UX Designer

Responsibilities of a UX Designer include:

* UX Discovery: Informing engineers of the expected hours required to design a component, page, or interface.
* Wireframe Design: Designing accessible, consistent interfaces as intuitively as possible.
* UX Testing: Validating UX designs through user testing.

### Product Manager

Responsibilities of a Product Manager include:

* Product Design
    * Working in partnership with (or as) the [UX Designer](#ux-designer) to create the workflows for a product.
    * Setting vision for the features and user stories of a project.
* User Validation
    * Validating product design choices through user feedback and testing.

### Developer Advocate

Responsibilities of a Developer Advocate include:

* Monitoring the well-being of team members.
* Checking for indicators of burnout.
* Proposing and designing solutions to improve personal satisfaction and work-life balance for team members.

### Sales Mentor

Responsibilities of a Sales Mentor include:

* Reviewing sales metrics.
* Designing sales and related training curriculum for team members.
* Developing proposals to increase sales performance.

### Chief Technology Officer (CTO)

* **Technical Arbiter**
    * Final arbiter for technical decisions
* **Technical Excellence**
    * Ensuring technical & product quality of everything we do
    * Ensuring software we produce is as open as possible and everything that can be contributed upstream is
    * Championing and improving processes (like our code review process) to improve the quality and efficiency of our software development
    * Reviewing code (in addition to the regular code review process), mentoring developers
    * Being aware of the "big picture" to help developers avoid duplicating each other's work
    * Being available on call 24/7 to respond to emergencies, if regular [firefighters](#firefighter) are not available
* **Epic/Client Owner Coach**
    * Supporting epic and client owners in client meetings, discussions, and in turning requirements into actionable plans/epics
    * Reviewing all discovery stories and estimates
* **Security Manager**
    * Ultimately responsible for security of our software, both proactively improving it and responding to incidents effectively

The CTO isn't part of a cell, but can review and participate in any cell's work as needed.

### Chief Executive Officer (CEO)

* **Business development**:
    * Additional relationship with key clients and prospects
    * Support role for the epic owner, in difficult situations or contract negotiations
    * Reviewer of quotes
    * Initiator for opportunities that are strategically important for OpenCraft, either
      because of size or side effects
* **Financials**:
    * Accounting
    * Invoicing (clients & team)
    * Forecasting
* **Admin**:
    * Holidays reviews
* **Legal**:
    * Relationship with lawyers
    * Contracts drafting and signature (team and clients)
    * Ownership of legal projects
* **Management**:
    * Management of non-technical matters for all cells
    * Management of CTO
    * 121 meetings: continuous rotation of all employees, at 2-3 meetings/week
    * Handle reports of performance issues & process to address it, including firing if needed
* **Hiring**:
    * Review of candidate selections by the team's recruitment manager
    * Contract negotiation with accepted candidates
* **Sprint management**:
    * Occasional second review of sprint deliverables, sprint planning & epics management on any
      cell
* **Strategy**: evolution of the company
* **Ocim**: product management and some code
* **Spreading the word** (mailing lists, conferences, opencraft.com)

### Marketing Specialist

As a **marketing specialist**:

* I will discuss and plan marketing strategies with the business development specialist, the CEO, and external marketing contractors
* I will schedule and coordinate tasks related to the agreed-upon marketing strategies
* I am responsible for coordinating maintenance, improvement, and all tasks related to the [OpenCraft website](https://opencraft.com)
* I am responsible for coordinating the publication of articles on OpenCraft's [blog](https://opencraft.com/blog)

### Administrative Specialist

As an **administrative specialist**:

* I will coordinate and execute all administrative tasks that are related to team member billing, client billing, invoice payments, bookkeeping, and other accounting-related matters at OpenCraft
* I will participate in the recruitment and onboarding [process](../processes/recruitment.md#administrative-specialist-recruitment-tasks-details) of new team members, and collect all necessary information to set up their invoicing process

[SprintCraft]: https://sprintcraft.opencraft.com
