# Cell-Specific Rule Definitions

This page lists rule definitions that are specific to individual cells.

## Serenity (DevOps cell)

### Cell Size

To set an upper bound for the number of non-billable hours that Serenity can generate per month
(cf. [Sustainability Management](#sustainability-management)) its size is limited to 3 members
and will be kept constant for the foreseeable future.

### Firefighting

#### Rotations

Due to its small size Serenity does not explicitly designate two firefighters each sprint.
Instead, each member of the cell allocates a small amount of time for firefighting each sprint,
relative to their weekly commitments (in hours): The number of firefighting hours that each cell member
allocates should (roughly) match 7.5% of their committed hours. (This is similar to our current practice
for generalist cells, which are supposed to allocate 7.5% of their total capacity to firefighting each week).

For example, if members of the cell are committed to working 40h, 12h, and 35h per week, respectively,
their corresponding firefighting hours would be:

```
0.075 * 40h/week = 3h/week
0.075 * 12h/week ~= 1h/week
0.075 * 35h/week ~= 3h/week
```

This would result in a total of 14h of firefighting time for each 2-week sprint.

#### Delegation

Tasks that exceed the cell's capacity for firefighting are delegated to firefighters of generalist cells.

#### Responsibilities

Firefighting responsibilities for Serenity do not include addressing issues affecting client instances,
unless there is an underlying infrastructure problem that is causing these issues and affecting multiple instances at once.

### Roles

#### Cell Management

* Responsibilities belonging to the Sprint Planning Manager and Sprint Manager roles are reduced
  to a subset of items that is relevant for Serenity. The two roles are assigned to the same person.
* Responsibilities belonging to the Epic Planning and Sustainability Manager role are reduced
  to a subset of items that is relevant for Serenity.

##### Sprint Planning Manager

* **Task Estimation Session**: [Default responsibilities](list.md#sprint-planning-manager) apply.
* **Prioritization**: Being a small cell that focuses exclusively on internal infrastructure-related projects,
  the DevOps cell should maintain a list of current and upcoming epics in a dedicated document
  (such as [Ocim & DevOps Epics Schedule (v2)](https://docs.google.com/document/d/1Fy-3au6-WbjpM4a_i_RtCV6ba_XdA29dcjctM9bSEkA/edit)),
  so that it is easy for other team members to get a high-level overview of what the DevOps cell is currently working on,
  as well as future plans.
    * The Sprint Planning Manager checks this document once per sprint and updates it to reflect the latest status and priorities.
    * On Friday before the start of a new sprint (when all relevant tasks for the upcoming sprint are available),
      the Sprint Planning Manager arranges the tasks scheduled for the next sprint to match current priorities
      as laid out in the document.
    * Additional tasks related to team-wide conversations and/or activities (such as discussing a process change on the forum or preparing for an upcoming conference) are slotted in as necessary.
* **Task Insertion**: [Default responsibilities](list.md#sprint-planning-manager) apply.
* **Sprint Preparation**: [Default responsibilities](list.md#sprint-planning-manager) apply.

##### Sprint Manager

* **Sprint Preparation**: Every Monday before the start of the new sprint:
    * Double-check that each member of the cell has allocated a small amount of time for firefighting, relative to their weekly commitments (in hours).
* **Assigning Rotations**: Not applicable (due to its small size Serenity does not keep a [rotation schedule](#rotations) for firefighting).
* **Mid-sprint updates**: [Default responsibilities](list.md#sprint-manager) apply.
* **Sprint Wrap-Up**: [Default responsibilities](list.md#sprint-manager) apply.
* **Meetings**: Not applicable (Serenity does asynchronous planning).

##### Epic Planning and Sustainability Manager

###### Epic Planning

[Default responsibilities](list.md#epic-planning) apply, with the following modifications:

* Since [cell size](#cell-size) will not change, it is not necessary to coordinate
  with the team's [recruitment manager](list.md#recruitment-manager) on a regular basis.
  The main scenario where help from the recruitment manager would be needed is
  if a member of the DevOps cell decides to leave the team: In that case,
  the epic planning and sustainability manager would work with the remaining cells
  to see if they could provide a replacement, and notify the team's recruitment manager
  of the capacity reduction so that they can take necessary steps to replenish the team's capacity
  (either by recruiting directly for the DevOps cell or by finding newcomers
  for the cell that donated one of their members to the DevOps cell).
* Since Serenity doesn't own any clients, adjusting client budgets in SprintCraft is not necessary.
* The epic planning and sustainability manager should use the modified checklist included [below](#bi-weekly-update-template)
  when compiling bi-weekly epic planning and sustainability updates.

###### Sustainability Management

As mentioned [above](#cell-size), Serenity (as the DevOps cell) is limited to 3 members
and its size will be kept constant for the foreseeable future. This sets an upper bound
for the number of non-billable hours that Serenity can generate per month:
the sum of the monthly hour commitments of its members, plus a small percentage of overtime.

As a result, responsibilities for sustainability management are reduced to making sure that the total number
of non-billable hours that Serenity accumulates each month does not exceed 348h, i.e., the total monthly
commitment of Serenity's current members. (Note that this cap does not include an additional buffer
for overtime: We assume that reasonable amounts of overtime incurred over the course of some sprints
will be offset by periods of less total time worked over the course of other sprints.
If it turns out that this is not the case we will revise the budget cap in the future.)

The sustainability manager should check the number of non-billable hours for the current month
once per sprint so that any issues can be caught early and necessary adjustments can be made
in the following sprint.

Aside from making sure that non-billable hours stay within the monthly limit mentioned above,
Serenity's sustainability manager should perform an [accounts review](../processes/epic_planning_and_sustainability_management.md#every-month)
once per month (scheduled for the same day as the accounts review for other cells). The main
aspect to focus on in the context of the DevOps cell is making sure that tickets dealing with
incidents affecting multiple client instances are associated with the clients' *Instance Maintenance*
accounts instead of internal DevOps accounts.

###### Bi-Weekly Checklist

The bi-weekly checklist for compiling epic planning and sustainability updates for Serenity looks like this:

```text
h3. Epic planning and sustainability update (Sprint ???)

h4. Epic status

* ( ) Review and update epic statuses as needed.
** ( ) New epics ("Prospect", "Offer / Waiting on client", "Accepted"):
*** ( ) Add an entry to the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.
*** ( ) Move to "In Development" if work has started.
** ( ) In development:
*** ( ) Make sure epic updates are posted by epic owner/reviewer.
*** ( ) Move epics that have become blocked on other work to "Offer / Waiting on client".
** ( ) Delivered epics:
*** ( ) Move to "Delivered to client / QA".
** ( ) Completed epics:
*** ( ) Move to "Done".
*** ( ) Update corresponding entries on the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.

* ( ) Compare the Epics board with the epic planning spreadsheet and update the spreadsheet as necessary.

h4. Availability updates (departing members, weekly commitments, vacations)

* ( ) If someone decides to leave the team:
** ( ) Update their availability for the coming months.
** ( ) Help find new owners for their responsibilities (roles, clients, epics).
** ( ) Find someone to replace them by coordinating with the remaining cells and/or the recruitment manager.
** ( ) **External replacement** (newcomer):
*** ( ) Add availability and onboarding hours for the coming months to the epic planning spreadsheet.
** ( ) **Internal replacement** (existing member):
*** ( ) Move availability from the donating cell's planning sheet to the DevOps cell's planning sheet.
*** ( ) Help find new owners for their responsibilities (roles, clients, epics).

* ( ) Handle requests from cell members to change their weekly hour commitments.

* ( ) Check the calendar for vacations coming up in the next couple months. If someone will be away for a total of 1 week (or longer), [post a comment mentioning their availability|https://gitlab.com/opencraft/documentation/public/merge_requests/99#note_198626533] in the epic planning spreadsheet.

h4. Capacity requirements

_For in-progress epics:_

* ( ) Evaluate the amount of time required to complete the accepted project scope over the next months and update capacity allocations in the epic planning spreadsheet.
* ( ) Ensure delivery and bugfix deadlines of individual epics are on target (or are being actively discussed on the epics). If that's not the case, comment directly on the epic planning spreadsheet, pinging epic owners as necessary.
* ( ) Ensure the cell's projects are being properly delivered using the epic management process.

h4. Budgeting

_Once per sprint:_

* ( ) *Monday of W3:* Check number of non-billable hours logged for the current month to see if cell is on track to stay below monthly budget cap (348h), and follow up as necessary.

_Once per month:_

* ( ) *Before the 5th of the month:* Check that tasks from previous month use correct accounts, and post update on your cell's epic planning and sustainability epic.
* ( ) Add actuals for previous month to epic planning spreadsheet.

h4. Notes

...
```

### Vacations

Due to its small [size](#cell-size) Serenity's limit for the percentage of members that can be away
at the same time is increased from 20% to 33%.
