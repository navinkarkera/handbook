# Vacation

The following rules apply to all cells by default.  Project cells have the option of specifying [their own](#project-cells), but if they don't do so explicitly, these ones will apply.

## Requesting time off

* Make sure to book your vacation:
    * For 1 day off, 3 days in advance
    * For 2 days to 1 week off, by Thursday evening of the week preceding the sprint in which time off starts
    * For more than a week off, two weeks in advance.
* Check the [team calendar](https://www.google.com/calendar/embed?src=auscaqbrvc0uatk6e7126614kk%40group.calendar.google.com)
  first to see if anyone else has booked those dates off. To ensure maximal availability and
  responsiveness within the cell, if the number of persons corresponding to 20% of the cell size
  (rounded up) are already away on some days, you can't book the same days. There is however
  a tolerated 1 day/week per person that can overlap. If in doubt, ask Xavier.
* If you decide to travel and still work, it's useful to let everyone know, but it's not necessary
  to go through the vacations process. As long as you have a good internet connection and maintain
  work hours and communication / reactivity levels, you can work from anywhere.

### Additional steps for core developers

* Check if you need to make any trades on the
  [rotations schedule](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit#gid=447257869)
  and if so, get tentative agreement from someone to trade with you. If the spreadsheet doesn't
  yet cover the week(s) you'll be away, there is a place near the bottom where you can add a note,
  so that we can account for your vacation when the schedule gets updated.
* Make sure that the epic reviewer for any epic you own is going to be present and is fully
  informed so they can take over the epic owner duties while you are away.

### Special vacation

Besides the "normal" vacation, we also offer the following two variants:

* *Reduced time off*: A temporary reduction of the number of hours you work each week.
* *Scoped time off*: A temporary change of the scope of work you will work on, to reduce availability to/from
  specific projects, clients or roles. This is usually used to allow to focus on one or several specific
  projects for a time, or to take time off projects without being fully off.

The same process and limit as for normal vacations applies. Simply also mention the amount of time that
you will have available, or your exact scope of work, for that period of time while you announce your vacations.

### Sick days

* When you are sick and unable to work, let the rest of the team know asap using the
  [Announcements forum category](https://forum.opencraft.com/c/announcements/7), and let people
  know where you would need help/cover as much as possible. In particular, mention which tasks
  are at risk of spillover or have a looming deadline.
* Then, rest! Don't half work while sick - it will take you longer to recover, and your work will
  likely not be very good.
* Note that it is also possible, and encouraged, to take
  [sick days for mental health](https://doist.com/blog/mental-health-and-remote-work/), when you need it.
  Just like other sick days, use them to rest and disconnect - don't check emails or tickets!

## Announcing your vacation

* Once you have confirmed everything, post on the [vacation thread on the forum](https://forum.opencraft.com/t/vacation-announcement-thread/145)
  to announce your vacation, including all the details:
    * Dates of the vacation (inclusive - ie May 1-3 would be 3 days off)
    * Who is covering for you, for each active epic, rotation and role you have
      responsibilities for
* Add an event on the OpenCraft calendar, for the period of the vacation, with:
    * Your name, followed by a colon
        * **Note:** if there is another member of the team with the same first name, you need to provide your full name in the calendar events.
    * Whether you are off, or on reduced time (and if so, how much)
        * Example supported formats: `John: off`, `John: available 4h`.
        * **Note:** do not add the reduced capacity events to your off days (e.g. weekends), because then this time will be counted as **negative** vacations (i.e. extra availability).
    * A link to the forum post
* Note that the announcement with the proper details is what confirms the vacation time. You
  are responsible for ensuring that your vacation time respects the rules - if it doesn't, then
  the time isn't confirmed as off, and the announcement will need to be updated, on both the forum
  and the calendar. In case of doubt, don't hesitate to ask Xavier for a review.

## Project cells

Because project cells are tailored to specific project needs, they are not required to subscribe to the default cell rules that govern member availability.  For instance, a four-person cell in an initial development phase may establish that short absences that would otherwise break the 20% rule are allowed.  (On the other end of the spectrum, a cell with a strict client SLA may want to expand on its [firefighting rules](../roles/list.md#project-cell-firefighting) so that more timezones are adequately covered.)

If the rules are different from the default, they must be explicitly mentioned in the list of
[Cell-Specific Rules](../cells/cell_specific_rules.md). If no custom vacation rules are defined there,
the default cell rules apply.

There are constraints, though:

* The project cell's vacation rules, if different from the standard, must be reviewed and approved unanimously by all of its members and the CEO during the cell's inception, and at any time thereafter when a change is proposed, as a change request to this handbook.
* The rules must always provision for self-approval of vacation requests without ambiguity, like the default vacation rules, and not require to seek explicit approval from another cell member. The rules can still specify _dependencies_ on other cell members, like ensuring to find backups, but we don't want vacation rules to depend on the subjective approval of a member - the rules themselves should explicitly encapsulate all the constraints to be considered when taking vacation, and empower individual cell members to evaluate them by themselves.
* The default OpenCraft client-facing SLA always applies.  In other words, the 24-hour reply rule continues to apply if not superceded by a stricter SLA, so a cell with few members needs to make sure that the client is being adequately supported when some of its members are on vacation.

## Vacation checklist

Checklists to go through before going on vacation for each type of role, as well as checklists for backups of a specific role during the vacation.

### Developer (generalist cell)

Follow the steps outlined in the [Vacation Checklist for Developers](https://app.process.st/workflows/Vacation-Checklist-for-Developers-vOTKZVD1cC0LF7N73VZG2w/run-link).

### Developer (project cell)

If steps are defined for your project cell in the appropriate section of the [Cell-Specific Rules](../cells/cell_specific_rules.md) page,
follow those.  If not, follow the same checklist as a [developer in a generalist cell](#developer-generalist-cell).

### CEO

#### A) Before going away

1. Warn:
    1. Team: send an email with the dates and give/remind emergency contact info
        1. Including CTO as backup
        1. If Business Development Specialist will be away at the same time, ask Admin or Marketing Specialist to monitor contact@
        1. If there is ongoing recruitment, warn the recruitment backup & ensure access to contract tool
    1. Clients & prospects, as necessary
    1. Accountants & lawyers, as necessary
1. Meetings:
    1. Schedule a preparation meeting on the last day with the CTO
    1. Mark other meetings as "no" and block time in calendar to prevent calendly from adding new meetings
    1. Tell backup what to attend, and cancel others (eg. client meetings)
    1. Set autoresponder, mentioning backup contact
    1. Update the [pager schedule]
1. Invoices (team, clients) - check in advance for vacation in the team, to see if some people need to send an invoice earlier

[pager schedule]: https://opencraft.app.eu.opsgenie.com/teams/dashboard/1114af08-343b-40e0-a9a3-37d56cfb37f8/main

#### B) Main backup (CTO)

1. Announce upgrade to latest version of Open edX if one is released during this period.
   To prepare in advance, and post on the same or next day it is released, as a reply on the
   edx-code@ mailing list thread by edX announcing it, and on the OpenCraft.Hosting newsletter.

#### C) Business Backup - Business Development Specialist

1. Prospects management: replying, meeting them, providing quotes & terms, scheduling meetings
   for when CEO is back if needed (via Calendly)
1. Handle email arriving in contact@opencraft.com

#### D) Administrative Backup - Admin Specialist

1. Recruitment: handling candidate interviews and contract signature
1. Legal and administrative tasks: handle discussions with legal and clients, and contact CEO in case of emergencies

### CTO

#### A) Before going away

1. List the reponsibilities and tasks of the main backup (see B)
1. Identify backups & create/clone tickets assigned to them, linked to the checklists on this page:
    1. Main backup (CEO)
    1. Sprint reviewer
    1. Ops reviewer - add to ops@opencraft.com and to the pager
1. Warn:
    1. Team: send an email with the dates
    1. Each current client in advance, as well as the edX Open Source team; tell them to contact the
       backup for emergencies
    1. Email: Set auto-responder, mentioning backup contact
    1. Pager: Update the [pager schedule]
1. Sprint/epic knowledge transfer:
    1. Review any tasks in "External Review/Blocker" or "Long External Review/Blocked" that are
       assigned to the CTO and transfer knowledge to someone else on the team
    1. Update each active epic description & write final update in the tickets comments before
       leaving, for knowledge transfer: status, schedule, plans, concerns, commitments, etc.
1. Meetings:
    1. Schedule review & pre-planning meeting on the last day (involve sprint reviewer)
    1. Review all upcoming meetings during time off and ask if backup can attend or the meeting
       organizer knows nobody from OpenCraft can make it.
    1. Schedule review & pre-planning meeting on the last day (involve sprint reviewer)
    1. Ensure time is marked as "unavailable" in Google Calendar and Calendly
    1. Review OpenCraft meeting lead schedule and trade meeting times with others as needed
1. Prepare an invoice for the current month, if the end of the month (invoice time) will happen
   during the vacation.

#### B) Main backup

1. Sprint planning:
    1. On Thursday and/or Friday of the second week of a sprint, work with epic owners to review
       the priority of all tickets in the backlog for the upcoming sprint.
    1. Subscribe to GitHub notifications for each PR attached to issues in "Long External
       Review/Blocked", so that you know if upstream starts reviewing them.
1. Sprint supervising:
    1. Check on individual task ETA during sprint to ensure completion
    1. Keep clients informed of progress & answering their questions
    1. Help to unblock anyone who is unable to work on their ticket
1. Attend meetings with potential or current clients to provide technical insight, planning, and estimates
1. Handover completed projects to the clients

### Business Development Specialist

#### A) Before going away

1. Warn Marketing Specialist about upcoming prospect work/quotes
1. Backup for contact@ email : Marketing Specialist, if also on vacation then ask CEO or CTO
1. Do important client follow-ups, let them know about vacation and who will answer them in the meantime
1. Last day:
    1. Set autoresponder, mentioning backup contact

#### B) Main backup

1. Answer contact@ emails
    1. Greet leads
    1. Schedule discovery task(s) if needed
    1. Prepare & share quotes
1. Review all emails in spam folder
1. Answer emails and support request from Pro & Teacher users
