# Sales

Sales and Business Development at OpenCraft is primarily driven by building a solid reputation for our services that ripples out into referrals who we then court.

That is not to say that advertising does not play a role in our overall marketing strategy, or that marketing overall is not important. However it does mean that our approach to handling sales and business development is in alignment with our [values](../../values.md) of openness, quality, and commitment. Of particular usefulness during sales is our quality of empathy: Not only is it important to understand how we can help our clients achieve their goals, it is also important to know when we are not the best fit for a client's needs.

We do not make promises we cannot keep. We set realistic expectations with prospects about what we will deliver, at what cost, and when. We are leaders in our field and do not skimp on quality.

## An Overview of a Sale at OpenCraft

Most sales at OpenCraft begin either by referral or through contacting us through our website. The client reaches out to us asking for more information, and we respond with an email giving a basic overview of OpenCraft and what we do. After initial exchanges, the [Business Development Specialist](../../roles/list.md#business-development-specialist) schedules a [meeting](#prospect-intro-meetings) with the client and gets an overview of their project.

Most clients select OpenCraft because they will need customization. This entails discovery to determine final costs. The business development representative contacts one of the developers on [Discovery Duty](../../roles/list.md#discovery-duty-assignee) and assigns them a task to figure out how to address their needs and what it will cost to do so.

After completion of the discovery, the Business Development Specialist integrates the figures determined by the developer into a full proposal document for signature, which is sent alongside the developer's discovery document. [Here is an example proposal](assets/institutional_plan.pdf) for an institution that wants hosting via our Institutional Plan, and also wants a custom 'Wormhole XBlock' to be developed.

The customer then reviews these documents, and, if satisfied, signs. The Billing Specialist onboards the client. The developer who performed the discovery takes charge of the client's epic. If the developer cannot take on the epic, then the business development specialist starts a forum post to see who from the core team is up for taking the client. Once final epic assignment has been completed, the epic owner begins scheduling tasks, and the development cycle is underway.

[A full view of the customer lifecycle is available here.](customer_lifecycle.md)

## Prospect Intro Meetings

As part of qualifying leads and better understanding how we may meet the needs of clients, initial introductory meetings are scheduled.

### Meeting Preperation

Before the meeting, search for information on the client online. Most organizations will have a website where you can learn about who they are and what they do. Looking up the contact's information on LinkedIn is also helpful.

Things to keep in mind when looking up the client's info:

1. Situation: What is the organization like? What do they do? Do we know of any trends or changes to their sector that may be affecting them?
2. Problem: Based on what they may have already communicated, or what you can infer from your experience with similar clients, what kinds of challenges might they be facing?
3. Implications: How might these problems be impacting what they're trying to accomplish as an organization?
4. Need Payoff: How might OpenCraft be uniquely suited to addressing their challenges in a way that helps them be more successful?

Spend some time ruminating over this information and bring it to the meeting. You can quickly build rapport by speaking to their needs. Write up what you find in an update on the CRM.


### Meeting agenda

While these meetings are fluid and should not have a strict structure, there are some helpful anchor points to keep in mind.

1. If you're not sure how familiar the client is with OpenCraft, ask if they'd like a basic rundown of who we are and what we do. If they say yes, give them a basic introduction, going over these points:
    * Our experience and knowledge of the Open edX platform
    * Our involvement in the Core Contributors program
    * Our commitment to Open Source
    * A summary of clients we work with
    * The volume of contributions we've made (and continue to make) to the platform
2. Preferably, you'll have already read up a little on their organization. Ask them about it, especially any specific questions you may have come up with when browsing their website.
3. Ask about the problem they're trying to solve.
4. At some point in the meeting (if it doesn't come up in 1), be sure to mention our policy of upstreaming contributions. The earlier we can gauge the client's appetite for open source, the sooner we can determine their people profile, and whether they'll be asking us to work under terms we'll accept.
5. If they have not tried Open edX yet, suggest that they sign up for the [P&T trial](https://opencraft.com/hosting/) so they can play with the software and see how it works. If you think it would be helpful, or if they've asked for a walkthrough, [bring up the demo instance](https://demo.opencraft.hosting/) and show them around.
6. Determine next steps and lay them out for post-meeting follow-up. This might be getting them to send a detailed list of requirements, or starting the discovery process. If starting a discovery, make sure to get them to agree to pay for the time spent on discovery. If they're a high priority lead, this cost can be deferred until they agree to start the project-- though you may have to guess at this point if you haven't already run the rubric.

When the meeting is finished, write an email to the client summarizing what you went over in the meeting, and what the next steps are. Create tasks as necessary and update the prioritization for the lead [in the CRM](https://opencraft.monday.com/boards/1042640419) so you can remember to check in later if they don't respond. Add an update to the lead, as well.

## Small Projects

Occasionally, a lead will come in asking for a single feature change to the platform.  The feature is apparently small and the person or team is not looking for an extended engagement.

In cases like these, the client can be offered two choices:

1. Run a discovery of up to five hours of billable time to create a blueprint of how long the feature will take and a final estimate.
2. Have them agree to up to X hours of development (no fewer than 10) to see how far we can get on the feature. Deliver what we can if we don't complete it in that time.

Option 1 is unlikely to be chosen for these cases. Smaller features might take just as long to do a standard discovery as it would be to build them. Option 2 should be a ballpark estimate. Increase this estimate if you foresee any issues.

## Request for Proposal (RFP) and Tender Projects

While most of our clients approach us to ask for quotes, a subset of companies we sell to publish a 'Request for Proposal' (private companies) or a 'Tender' (public ones) which are a general call for bids from vendors.

Tenders and RFPs are unique in that they require upfront investments from our team in order to submit our bids. In these cases we are unable to bill out the discovery time if we are not chosen. Sometimes, we cannot bill out even if we **are** chosen. To that end, we have some rules for engaging requests for proposals.

1. We do not respond to RFPs and tenders for any group which does not score into the High Priority category of our lead prioritization rubric. All other organizations must go through the normal discovery process, or we will decline to work with them.
2. If a lead does score into the high priority category, we will require payment for the discovery work upon acceptance of the terms.
3. ...With one narrow exception. If the legal dictates of a government client's tender process prevent us from billing for discovery work, we will not bill for it. This will need close assessment from the business development representative to verify that the risk of sinking time into the tender is worth the cost. Private and nonprofit entities, as categories, will not be allowed to forgo discovery charges after acceptance.


## Writing Proposals

All proposals should capture the full breadth of the discovery process costs. Meetings, discovery work, management overhead-- all of these need to be factored into the final price. The [discovery process](../../how_to_do_estimates.md) gives us a good view of what these costs will be.

Proposals are generated using [Proposify](https://proposify.com/), sometimes with accompanying documents. [Here is an example proposal.](assets/institutional_plan.pdf)

If a project is fixed-cost (that is, not to be long-term or iteratively developed), the proposal should contain the full estimated hours of the work, including time spent on discovery if it has not already been billed. Including the estimates sheet from the discovery process is recommended. You should always send the main discovery document.

If it is to be iteratively developed, the proposal should include the expected monthly development hours in one of the time banks at the bottom of the proposal template, rather than a lump sum. The proposal should be accompanied by the estimates spreadsheet, and the main discovery document, as always. Depending on the size and length of the engagement, a supplementary sheet showing year-by-year budget needs may also be necessary-- in most cases a client will ask for this if required.

Additionally, always send a link to [this article](https://opencraft.com/blog/how-agile-methodology-impacts-planning-and-budgets/) to clients who will be engaging in iterative development with us, and make it clear that as we learn more about the project, the actual costs, features, and other work, are likely to change. Emphasize that estimates are based on what we know at this time only.

### Transparency in Pricing

Including the estimates spreadsheet is important to make pricing transparent. While it is possible to 'roll' discovery and management hours into individual development tasks to pad them for required overhead, this makes a full understanding of how resources are to be spent difficult. It also requires a lot of manual work from the proposal writer to 'make everything fit.' It's better to dispense with this and lay out the costs out as transparently as possible.
