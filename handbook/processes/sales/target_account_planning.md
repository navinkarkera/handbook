# Target Account Planning

OpenCraft's clientele has historically come through word of mouth. As we grow, we are beginning to target specific companies we're interested in working with. These are companies that would score highly on our [Lead prioritization rubric](lead_prioritization.md) but which haven't reached out to us first.

At the moment, we do not have well-defined processes around target accounts, but we do have a few tools. The first of which is [our CRM](https://opencraft.monday.com/boards/1525054205) for tracking the target accounts we are currently pursuing. This board allows us to keep track of the accounts and our primary contacts within those teams.

The second is our planning account template. This can be found by creating a new Google doc from a template, which you can find in the [Template Gallery](https://docs.google.com/document/u/0/?tgif=d&ftv=1) in Google Docs. Once you've created this document, link it to the entry on the board using the 'Planning Link' field.

These documents should be stored in the OpenCraft shared drive, under [BizDev->Target Accounts](https://drive.google.com/drive/u/0/folders/1d_68yVh8ZV8WiLmPNJ7Pajum8qEC8LN4).

The target planning account template will aid you in researching target accounts and finding out how our services would benefit them, as well as how to approach them.