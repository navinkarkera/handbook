javascript:( async function(){

    /* Set your cell here */
    const cellProjectKey = "MNG";

    /* This is a bookmarklet. */
    /* Copy it, edit your cell above, and then create a bookmark in your favorite browser and paste this into the URL field. */
    /* Then from any issue in JIRA, click this bookmark to automatically create a CC Review ticket for it. */

    /* Get the current issue's account (numerical ID, not just key) */
    const accountKey = document.querySelector("#customfield_10011-val a").href.replace(/.*key=/, "");
    if (!accountKey) { throw new Error("Couldn't detect account ID"); }
    const accountData = await (await fetch(`https://tasks.opencraft.com/rest/tempo-accounts/1/account/key/${accountKey}`, {credentials: 'same-origin'})).json();

    /* Get the current issue's epic key (e.g. "SE-5021") */
    const epicKey = document.querySelector("#customfield_10006-val a").href.replace(/.*browse\//, "");
    if (!epicKey) { throw new Error("Couldn't detect epic ID"); }

    /* Create a new JIRA issue */
    const w2 = window.open();
    w2.document.write('<body>Creating new issue...</body>');
    const response = await fetch(`https://tasks.opencraft.com/rest/api/2/issue/`, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            fields: {
                project: {key: cellProjectKey},
                issuetype: {name: "Story"},
                summary: 'CC Review: ' + document.title.replace(' - OpenCraft', ''),
                /* In the description, just link to the original issue */
                description: window.location.href,
                assignee: { name: JIRA.Users.LoggedInUser.userName() },
                /* Set reviewer equal to assignee */
                customfield_10200: { name: JIRA.Users.LoggedInUser.userName() },
                /* Set epic to the same as the original ticket */
                customfield_10006: epicKey,
                /* Set the account (given its ID but must be given as a string) */
                customfield_10011: `${accountData.id}`,
                /* Add the "CoreContributor" label */
                labels: ["CoreContributor"],
            },
        }),
    });
    if (response.status === 201) {
        const responseData = await response.json();
        console.log(`Created issue ${responseData.key}`);
        /* Update the issue links */
        await fetch(`https://tasks.opencraft.com/rest/api/2/issueLink`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                type: { name: "Blocks"},
                inwardIssue: { key: JIRA.Issue.getIssueKey() },
                outwardIssue: { key: responseData.key },
            }),
        });
        w2.location.href = `https://tasks.opencraft.com/browse/${responseData.key}`;
    } else {
        alert("Failed to create new issue");
        w2.close();
        console.error(await response.json());
    }
} )()
