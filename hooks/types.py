"""
Types used by hook functions, not for defining the organization schema.
"""
from typing import TypedDict, List, Dict

from hooks.schema import KNOWN_POSITIONS, KNOWN_ROLES, RawCell, MetaCell


class MembersRolePosition(TypedDict):
    """
    List of members and their position in a role.
    """
    members: List[str]
    position: KNOWN_POSITIONS


PositionMembers = Dict[KNOWN_POSITIONS, List[str]]


class RoleAssignment(TypedDict):
    """
    Refined roles assignment listing consolidating all positions.
    """
    name: KNOWN_ROLES
    positions: List[MembersRolePosition]


TeamRoles = Dict[KNOWN_ROLES, Dict[KNOWN_POSITIONS, List[str]]]


class RefinedCell(RawCell):
    """
    Structure of a cell after all roles have been reassembled.
    """
    roles: List[RoleAssignment]


class CellsByType(TypedDict):
    """
    Structure for accessing cell definitions by their type.
    """
    Generalist: List[RefinedCell]
    Project: List[RefinedCell]
    Support: List[RefinedCell]


CellRolesByName = Dict[str, Dict[KNOWN_ROLES, PositionMembers]]


class RefinedOpenCraft(TypedDict):
    """
    Post-processed version of the organization useful for
    templating.
    """
    cells: List[RefinedCell]
    meta_cell: MetaCell
    roles: List[RoleAssignment]


CellsByName = Dict[str, RefinedCell]


class TeamMappings(TypedDict):
    """
    Useful mappings for compiling team information.
    """
    cells_by_name: CellsByName
    cell_roles_by_name: CellRolesByName
    team_roles: TeamRoles
