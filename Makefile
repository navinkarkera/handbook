# Makefile for OpenCraft handbook.
all: install_prereqs quality compile

compile:
	mkdocs build

preview:
	mkdocs build --no-directory-urls --site-dir preview

install_prereqs:
	pip install pip-tools==6.4.0
	pip-sync
	# Run it a second time. Something's wrong with the package management here, but it seems to be upstream.
	pip-sync
	pyenv rehash || echo "Pyenv is broken or non-existent. Ignoring."

upgrade:
	pip-compile requirements.in

quality:
	make quality-python
	make quality-markdown

quality-python:
	pylint hooks
	pycodestyle hooks
	mypy hooks

quality-markdown:
	npx markdownlint-cli handbook

run:
	mkdocs serve

clean:
	rm -rvf build

test:
	pytest --cov=hooks
